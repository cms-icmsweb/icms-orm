"""Add Regions Table

Revision ID: 2022.01
Revises: 2021.05
Create Date: 2022-05-03 10:50:57.066596

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2022.01'
down_revision = '2021.05'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'region',
        sa.Column('code', sa.String(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('code', name=op.f('pk_region')),
        schema='public'
    )
    op.add_column('country', sa.Column('region_code', sa.String(32), nullable=True))
    op.create_foreign_key(op.f('fk_country_region_code_region'), 'country', 'region', ['region_code'], ['code'], source_schema='public', referent_schema='public', onupdate='CASCADE', ondelete='SET NULL')


def downgrade():
    op.drop_constraint(op.f('fk_country_region_code_region'), 'country', schema='public', type_='foreignkey')
    op.drop_column('country', 'region_code')

    op.drop_table('region', schema='public')
