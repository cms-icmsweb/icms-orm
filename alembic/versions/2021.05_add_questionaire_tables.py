from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '2021.05'
down_revision = '2021.04'
branch_labels = None
depends_on = None

def upgrade():

    op.create_table('job_questionnaire',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(), nullable=False),
    sa.Column('questions', sa.String(), nullable=False),
    sa.Column('date_created', sa.DateTime(), nullable=True, server_default=sa.func.current_timestamp()),
    sa.Column('date_updated', sa.DateTime(), nullable=True, server_default=sa.func.current_timestamp()),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_job_questionnaire')),
    schema='public'
    )

    
    op.add_column('job_opening', sa.Column('questionnaire_id', sa.Integer(), nullable=True))
    op.create_foreign_key(op.f('fk_job_opening_questionnaire_id'), 'job_opening', 'job_questionnaire', ['questionnaire_id'], ['id'],
     source_schema='public', referent_schema='public', onupdate='CASCADE')

    op.add_column('job_nomination', sa.Column('questionnaire_answers', sa.String(), nullable=True))

def downgrade():
    op.drop_column('job_nomination', 'questionnaire_answers')

    op.drop_constraint(op.f('fk_job_opening_questionnaire_id'), 'job_opening', schema='public', type_='foreignkey')
    op.drop_column('job_opening', 'questionnaire_id')

    op.drop_table('job_questionnaire', schema='public')
