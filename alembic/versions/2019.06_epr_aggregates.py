"""EPR aggregates

Revision ID: 2019.06
Revises: 2019.05
Create Date: 2019-06-19 15:53:04.926882

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from icms_orm.common import DeclarativeBase
from icms_orm.common import epr_aggregate_units_enum

# revision identifiers, used by Alembic.
revision = '2019.06'
down_revision = '2019.05'
branch_labels = None
depends_on = None


def upgrade():
    epr_aggregate_units_enum.create(op.get_bind())
    op.create_table('epr_aggregate',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('cms_id', sa.Integer(), nullable=True),
    sa.Column('inst_code', sa.String(32), nullable=False),
    sa.Column('project_code', sa.String(32), nullable=False),
    sa.Column('year', sa.Integer(), nullable=False),
    sa.Column('aggregate_value', sa.Float(), server_default='0', nullable=False),
    sa.Column('aggregate_unit', epr_aggregate_units_enum, nullable=False),
    sa.ForeignKeyConstraint(['cms_id'], [u'public.person.cms_id'], name=op.f('fk_epr_aggregate_cms_id_person')),
    sa.ForeignKeyConstraint(['inst_code'], [u'public.institute.code'], name=op.f('fk_epr_aggregate_inst_code_institute')),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_epr_aggregate')),
    sa.UniqueConstraint('cms_id', 'inst_code', 'project_code', 'year', 'aggregate_unit', name='epr_aggregate_coordinates'),
    schema='public'
    )


def downgrade():
    op.drop_table('epr_aggregate', schema='public')
    epr_aggregate_units_enum.drop(op.get_bind())

