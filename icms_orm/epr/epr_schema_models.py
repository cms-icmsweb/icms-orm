#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from sqlalchemy import orm, Column, types, ForeignKey, schema
from sqlalchemy.orm import relationship
from datetime import datetime
from uuid import uuid4
from icms_orm import epr_bind_key, epr_schema_name, IcmsDeclarativeBasesFactory
from sqlalchemy_continuum import transaction
import traceback
import logging


DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(epr_bind_key())


class EprBaseMixin():
    __table_args__ = {'implicit_returning': True, 'schema': epr_schema_name()}
    __bind_key__ = epr_bind_key()


class VersionedMixin(object):
    __versioned__ = {}


def get_task_types():
    return [u'Perennial', u'One-off', u'InstResp', u'Qualification']  # , u'locked', u'non-core Perennial', u'non-core one-off']


def get_task_shift_type_ids():
    return [u'on-call shift', u'on-call expert shift', u'regular shift', u'central shift']


class Permission:
    READ       = 0x00000001

    MANAGEUSER = 0x00000002
    MANAGEACT  = 0x00000004
    MANAGEPROJ = 0x00000010
    MANAGETASK = 0x00000020

    MANAGEINST = 0x00000040

    OBSERVE    = 0x00000100

    ENGOFF_MGT = 0x00100000  # Management actions by people in the EngagementOffice

    ADMINISTER = 0x08000000


class EprInstitute(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'insts'

    id = Column(types.Integer, schema.Sequence('id_seq_inst'), primary_key=True)
    code = Column(types.String(80), unique=True)
    cernId = Column('cernid', types.Integer, unique=True)
    name = Column(types.String(200))
    country = Column(types.String(150))  # move to ISO country code ?
    cmsStatus = Column('cms_status', types.String(40), nullable=False)

    comment = Column(types.String(1000))
    year = Column(types.Integer)

    timestamp = Column(types.DateTime, default=datetime.utcnow)

    # an institute has many users, and users may have several (few) institutes
    users = None

    def __init__(self, cernId, name, country, code, cmsStatus, year=2015, comment=''):
        self.cernId = cernId
        self.name = name
        self.country = country.upper()
        self.code = code
        self.cmsStatus = cmsStatus
        self.year = year
        self.comment = comment
        self.timestamp = datetime.utcnow()

    def to_json(self):
        json_user = {
            'cernId': self.cernId,
            'code': self.code,
            'name': self.name,
            'country': self.country,
            'status': self.cmsStatus,
            'users': self.users,
            'comment': self.comment,
            'year': self.year,
        }
        return json_user

    @staticmethod
    def get_members_only():
        return EprInstitute.query.filter(EprInstitute.cmsStatus == 'Yes').order_by(EprInstitute.name).all()


class Role(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'roles'

    id = Column(types.Integer, schema.Sequence('id_seq_role'), primary_key=True)
    name = Column(types.String(64), unique=True)
    default = Column(types.Boolean, default=False, index=True)
    permissions = Column(types.Integer)
    users = orm.relationship('EprUser', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name


class Category(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'categories'

    id = Column(types.Integer, schema.Sequence('id_seq_cats'), primary_key=True)

    name = Column('name', types.String(80), unique=True)
    authorOK = Column('author_ok', types.Boolean)

    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, name, ok):
        self.name = name
        self.authorOK = ok
        self.timestamp = datetime.utcnow()

    def to_json(self):
        json_user = {
            'name': self.name,
            'authorOK': self.authorOK,
            'timestamp': self.timestamp
        }
        return json_user


class EprUser(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'users'

    id = Column(types.Integer, schema.Sequence('id_seq_user'), primary_key=True)
    username = Column(types.String(80))  # some users do not have CERN accounts, so this will be None
    name = Column(types.String(200))

    hrId = Column('hrid', types.Integer, unique=True)
    cmsId = Column('cmsid', types.Integer, unique=True)
    mainInst = Column('main_inst', types.Integer, ForeignKey(EprInstitute.id))
    institute = orm.relationship(EprInstitute)
    # institutes - backref from Institutes

    role_id = Column(types.Integer, ForeignKey(Role.id))

    authorReq = Column("auth_req", types.Boolean)
    isSuspended = Column('is_suspended', types.Boolean)
    status = Column('status', types.String(40))
    category = Column("category", types.Integer, ForeignKey(Category.id))
    category_rel = orm.relationship(Category)
    year = Column('year', types.Integer)

    comment = Column(types.String(1000))

    code = Column('code', types.String(80), unique=True)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    creationTime = Column('creation_time', types.DateTime, nullable=True)

    def __init__(self, username, name, hrId, cmsId, instId,
                 authorReq, status, isSusp, categoryName,
                 year, mainInst, comment='', role=None):

        self.username = username
        self.name = name
        self.hrId = hrId
        self.cmsId = cmsId
        self.mainInst = mainInst
        self.authorReq = authorReq
        self.status = status
        self.isSuspended = isSusp
        self.category = categoryName and self.session().query(Category).filter_by(name=categoryName).first().id or None
        self.comment = comment
        self.year = year

        self.code = str(uuid4())
        self.timestamp = datetime.utcnow()
        if role is None:
            if self.username in ['ap', 'andreasp', 'pfeiffer']:
                self.role = self.session().query(Role).filter_by(permissions=Permission.ADMINISTER).first()
            if self.role is None:
                self.role = self.session().query(Role).filter_by(default=True).first()
        else:
            self.role = role


    def grantRole(self, role):
        self.role_id = role.id

    def verify_password(self, password):
        return True  # check_password_hash(self.password_hash, password)

    def canManageUser(self, user, year):

        # check if the current user can manage another user (i.e. if the
        # current user is (deputy) institute/team leader of the user

        # get the actual parameters for the user and the selected year:
        try:
            tlUser = self.session().query(TimeLineUser).filter_by(year=year, cmsId=user.cmsId).order_by(TimeLineUser.timestamp).first()
        except Exception as e:
            if "No row was found for one" in str(e):
                return False  # no valid user for check
            else:
                msg = "Error from DB when trying to find user %s - got %s " % (user.username, str(e))
                print("canManageUser> ", msg)
                logging.error(traceback.format_exc())
                return False

        manager = Manager.query.filter_by(itemType='institute',
                                          itemCode=tlUser.instCode,
                                          userId=self.id,
                                          year=year,
                                          status='active').first()

        if manager and manager.role == Permission.MANAGEINST: return True  # found The One

        return False


    def is_administrator(self):
        return self.role is not None and \
               (self.role.permissions & Permission.ADMINISTER) == Permission.ADMINISTER


    def to_json(self):
        json_user = {
            'username': self.username,
            'name': self.name,
            'cmsId': self.cmsId,
            'hrId': self.hrId,
            'mainInst': self.mainInst,
            'suspended': self.isSuspended,
            'authReq': self.authorReq,
            'status': self.status,
            'code': self.code,
            'comment': self.comment,
            'year': self.year,
            'category': self.category,
        }
        return json_user


class Manager(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'managers'

    id = Column(types.Integer, schema.Sequence('id_seq_mgr'), primary_key=True)

    code = Column('code', types.String(80))

    itemType = Column('item_type', types.String(80))  # e.g. ExtAuthorProject, Activity, Task, Overall
    itemCode = Column('item_code', types.String(80))  # code of the project/activity/task
    userId = Column('userid', types.Integer, ForeignKey(EprUser.id), nullable=True)
    user = orm.relationship(EprUser, lazy='joined')
    role = Column(types.Integer)  # from Permissions
    year = Column(types.Integer)

    comment = Column(types.String(1000))

    status = Column(types.String(20))  # active or deleted (as we need to keep the history)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    egroup = Column(types.String(80), nullable=True)

    def __init__(self, itemType, itemCode, userId, comment='', year=2016, role=Permission.READ, egroup=None, timestamp=None):
        self.itemType = itemType
        self.itemCode = itemCode
        self.userId = userId
        self.role = int(role)
        self.status = 'active'
        self.year = year
        self.comment = comment

        self.code = str(uuid4())
        self.timestamp = timestamp or datetime.utcnow()
        self.egroup = egroup

    def to_json(self):
        json_user = {
            'itemType': self.itemType,
            'itemCode': self.itemCode,
            'userId': self.userId,
            'role': self.role,
            'status': self.status,
            'code': self.code,
            'year': self.year,
            'comment': self.comment,
            'egroup': self.egroup,
        }
        return json_user

    def __repr__(self):
        return '<Manager itemType: %s itemCode %s user %i role %s status %s code %s year %i comment %s egroup %s>\n' % \
               (self.itemType, self.itemCode, self.userId, self.role, self.status, self.code, self.year, self.comment,
                self.egroup)


class UsersInstitutes(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'userinst'

    id = Column('id', types.Integer, schema.Sequence('id_seq_userinst'), primary_key=True)
    user_id = Column('user_id', types.Integer, ForeignKey(EprUser.id))
    inst_id = Column('inst_id', types.Integer, ForeignKey(EprInstitute.id))

class Project(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'projects'

    id = Column(types.Integer, schema.Sequence('id_seq_proj'), primary_key=True)
    code = Column(types.String(80), unique=True)
    year = Column(types.Integer)

    name = Column(types.String(80))
    description = Column(types.String(1000))

    # activities : backref

    # not yet used
    maxTaskLevel = Column('max_task_level', types.Integer)

    # for internal book-keeping history
    # (don't update, add a new entry with the timestamp, then select most recent)
    timestamp = Column(types.DateTime, default=datetime.utcnow)
    status = Column(types.String(20), default='ACTIVE', nullable=False)

    def __init__(self, name=None, desc=None, code=None, year=2015, mxLvl=1):
        self.code = code
        self.name = name
        self.year = year
        self.description = desc
        self.maxTaskLevel = mxLvl

        self.timestamp = datetime.utcnow()

    def __repr__(self):
        return '<project id %i name %s description: %s code %s year %i>' % (self.id, self.name, self.description, self.code, self.year)

    def to_json(self):
        json_data = {
            'code': self.code,
            'name': self.name,
            'year': self.year,
            'description': self.description,
            'maxTaskLevel': self.maxTaskLevel
        }
        return json_data


class CMSActivity(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'cms_activities'

    id = Column(types.Integer, schema.Sequence('id_seq_act'), primary_key=True)
    code = Column(types.String(80), unique=True)
    year = Column(types.Integer)

    name = Column(types.String(200))
    description = Column(types.String(1000))

    # an activity belongs to one project
    projectId = Column('project_id', types.Integer, ForeignKey(Project.id))
    project = orm.relationship(Project, backref="activities")

    # tasks : backref

    # for internal book-keeping history
    # (don't update, add a new entry with the timestamp, then select most recent)
    timestamp = Column(types.DateTime, default=datetime.utcnow)
    status = Column(types.String(20), default='ACTIVE', nullable=False)

    def __init__(self, name=None, desc=None, proj=None, code=None, year=2015):
        self.code = code
        self.name = name
        self.year = year
        self.description = desc
        if proj:
            self.projectId = proj.id
            proj.activities.append(self)

        self.timestamp = datetime.utcnow()

    def __repr__(self):
        return '<activity id %i name %s year %i project %s:%s description: %s code %s >' % \
               (self.id, self.name, self.year, str(self.projectId), self.project.name, self.description, self.code)

    def to_json(self):
        json_data = {
            'code': self.code,
            'name': self.name,
            'year': self.year,
            'description': self.description,
            'projectId': self.projectId
        }
        return json_data


class Level3Name(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'level3names'

    id = Column(types.Integer, primary_key=True)
    name = Column(types.String(80), unique=True)

    startYear = Column(types.Integer, nullable=False, default=-1)
    endYear = Column(types.Integer, nullable=False, default=-1)

    comment = Column(types.String(1000))
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, name, startYear=-1, endYear=-1, comment=None):
        self.name = name
        self.startYear = startYear
        self.endYear = endYear
        self.comment = comment
        self.timestamp = datetime.utcnow()

    def to_json(self):
        jsonData = {'id': self.id,
                    'name': self.name,
                    'startYear': self.startYear,
                    'endYear': self.endYear,
                    'comment': self.comment,
                    'timestamp': self.timestamp,
                    }
        return jsonData

    def __repr__(self):
        msg = '<Level3Name: '
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % (k, str(v))
        msg += '>'
        return msg



class Task(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'tasks'

    id = Column(types.Integer, schema.Sequence('id_seq_task'), primary_key=True)
    code = Column(types.String(80), unique=True)

    year = Column(types.Integer)

    name = Column(types.String(200))
    description = Column(types.String(2000))

    # a task belongs to one activity
    activityId = Column('activity_id', types.Integer, ForeignKey(CMSActivity.id))
    activity = orm.relationship("CMSActivity", backref="tasks")

    neededWork = Column('needed_work', types.Float)

    # percentage of time needed at CERN
    pctAtCERN = Column('pct_at_cern', types.Integer)

    # e.g. Perennial, one-off, central shift, expert on call shift
    tType = Column('t_type', types.String(80))
    shiftTypeId = Column('shift_type_id', types.String(80))

    comment = Column(types.String(2000))
    earliestStart = Column('earliest_start', types.String(80))
    latestEnd = Column('latest_end', types.String(80))

    # not yet used
    level = Column(types.Integer)
    parent = Column(types.String(80))  # or id of parent task

    # for internal book-keeping history
    # (don't update, add a new entry with the timestamp, then select most recent)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    status = Column(types.String(20), default='ACTIVE', nullable=False)
    locked = Column(types.Boolean, default=False, nullable=False)
    kind = Column(types.String(20), default='CORE', nullable=False)

    level3 = Column('level3_id', types.Integer, ForeignKey(Level3Name.id))

    def __init__(self, name=None, desc=None, act=None, needs=None, pctAtCERN=None, tType=None, comment='', code='',
                 level3=None, year=2015, level=1, parent='', shiftTypeId=-1, earliestStart='', latestEnd='', status='ACTIVE',
                 locked=False, kind='CORE'):
        self.code = code
        self.name = name
        self.year = year
        self.description = desc

        if act:
            self.activityId = act.id
            act.tasks.append(self)

        self.neededWork = needs
        self.pctAtCERN = pctAtCERN
        if type(tType) == type(0):
            self.tType = get_task_types()[tType]
        else:
            self.tType = tType
        self.comment = comment
        self.earliestStart = earliestStart
        self.latestEnd = latestEnd

        self.level = level
        self.parent = parent
        self.shiftTypeId = shiftTypeId

        self.status = status
        self.locked = locked
        self.kind = kind

        self.level3 = level3

        self.timestamp = datetime.utcnow()

    def __repr__(self):
        msg = '<task id %s ' % str(self.id)
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % (k, str(v))
        msg += '>\n'
        return msg

    def to_json(self):
        json_data = {
            'code': self.code,
            'name': self.name,
            'year': self.year,
            'description': self.description,
            'activityId': self.activityId,
            'neededWork': self.neededWork,
            'pctAtCERN': self.pctAtCERN,
            'taskType': self.tType,
            'comment': self.comment,
            'earliestStart': self.earliestStart,
            'latestEnd': self.latestEnd,
            'shiftTypeId': self.shiftTypeId,
            'status': self.status,
            'locked': self.locked,
            'kind': self.kind,
            'level3' : self.level3,
            # 'level' : self.level,
            # 'parent' : self.parent
        }
        return json_data


class Pledge(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'pledges'

    id = Column(types.Integer, schema.Sequence('id_seq_pledge'), primary_key=True)

    code = Column('code', types.String(80), unique=True)

    userId = Column('user_id', types.Integer, ForeignKey(EprUser.id))
    instId = Column('inst_id', types.Integer, ForeignKey(EprInstitute.id))
    taskId = Column('task_id', types.Integer, ForeignKey(Task.id))

    workTimePld = Column('work_pld', types.Float)
    workTimeAcc = Column('work_acc', types.Float)
    workTimeDone = Column('work_done', types.Float)

    # to be updated by user (e.g. on request via email every three months):
    workedSoFar = Column('work_so_far', types.Float)

    status = Column(types.String(20))
    year = Column(types.Integer)

    # for internal book-keeping history
    # we don't update pledges once they have been accepted, instead we add a new
    # entry with the timestamp, then select most recent
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    isGuest = Column('is_guest', types.Boolean, default=False, nullable=False)

    def __init__(self, taskId, userId, instId,
                 workTimePld, workTimeAcc, workTimeDone,
                 status, year, workedSoFar=0, isGuest=False):
        self.code = '+'.join([str(x) for x in [taskId, userId, instId]])
        self.taskId = taskId
        self.userId = userId
        self.instId = instId
        self.year = year
        self.workTimePld = workTimePld
        self.workTimeAcc = workTimeAcc
        self.workTimeDone = workTimeDone
        self.workedSoFar = workedSoFar
        self.status = status
        self.timestamp = datetime.utcnow()
        self.isGuest = isGuest

    def __repr__(self):
        return '<pledge id %s task %s user %s (institute %s) pledged %f accepted %f done %f status %s for year: %s timestamp %s code %s isGuest %s>\n' % \
               (self.id, self.taskId, self.userId, self.instId, self.workTimePld, self.workTimeAcc, self.workTimeDone,
                self.status, self.year, self.timestamp, self.code, self.isGuest)

    def getKey(self):
        return self.code

    def to_json(self):
        json_data = {
            'code': self.code,
            'userId': self.userId,
            'instId': self.instId,
            'taskId': self.taskId,
            'workTimePld': self.workTimePld,
            'workTimeAcc': self.workTimeAcc,
            'workTimeDone': self.workTimeDone,
            'workedSoFar': self.workedSoFar,
            'status': self.status,
            'year': self.year,
            'isGuest': self.isGuest,
            'id' : self.id,
        }
        return json_data


class Shift(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'shifts'

    id = Column(types.Integer, schema.Sequence('id_seq_shifts'), primary_key=True)
    shiftId = Column('shift_id', types.Integer, unique=True)

    userId = Column('shifter_id', types.Integer, ForeignKey(EprUser.hrId))

    subSystem = Column('sub_system', types.String(100))
    shiftType = Column('shift_type', types.String(100))
    shiftTypeId = Column('shift_type_id', types.Integer)

    shiftStart = Column('shift_start', types.DateTime)
    shiftEnd = Column('shift_end', types.DateTime)
    flavourName = Column('flavour_name', types.String(20))
    weight = Column(types.Float)

    year = Column(types.Integer)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, shiftId, hrId, subSys, sType, sTypeId, sStart, sEnd, flavourName, weight, year):
        self.shiftId = shiftId
        self.userId = hrId
        self.subSystem = subSys
        self.shiftType = sType
        self.shiftTypeId = sTypeId
        self.shiftStart = sStart
        self.shiftEnd = sEnd
        self.flavourName = flavourName
        self.weight = weight
        self.year = year

        self.timestamp = datetime.utcnow()

    def to_json(self):
        json_data = {
            'shiftId': self.shiftId,
            'userId': self.userId,
            'subSystem': self.subSystem,
            'shiftType': self.shiftType,
            'shiftTypeId': self.shiftTypeId,
            'shiftStart': self.shiftStart.strftime('%Y-%m-%dT%H%M%S %z'),
            'shiftEnd': self.shiftEnd.strftime('%Y-%m-%dT%H%M%S %z'),
            'flavourName': self.flavourName,
            'weight': self.weight,
            'year': self.year,
        }
        return json_data


class ShiftGrid(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'shift_grid'

    shiftTypeId = Column( 'shift_type_id', types.Integer , nullable=False, primary_key=True )
    shiftType   = Column( 'shift_type', types.String(200), nullable=False )
    subSystem   = Column( 'sub_system', types.String(200), primary_key=True )
    orderNum    = Column( 'order_num', types.Integer , nullable=False, primary_key=True )
    startHour   = Column( 'start_hour', types.DateTime )
    endHour     = Column( 'end_hour'  , types.DateTime )

    def __init__(self, subSys, sType, sTypeId, startHour, endHour, orderNum):
        self.shiftType = sType
        self.shiftTypeId = sTypeId
        self.subSystem = subSys
        self.orderNum = orderNum
        self.startHour = startHour
        self.endHour = endHour

    def to_json(self):
        json_data = {
                'subSystem' : self.subSystem,
                'shiftType' : self.shiftType,
                'shiftTypeId' : self.shiftTypeId,
                'orderNum' : self.orderNum,
                'startHour' : self.startHour.strftime('%Y-%m-%dT%H%M%S %z'),
                'endHour' : self.endHour.strftime('%Y-%m-%dT%H%M%S %z'),
        }
        return json_data


class AllInstView(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'v_all_inst'

    id     = Column(types.Integer, schema.Sequence('id_seq_allinstview'), primary_key=True)

    # code and name of the institute
    code     = Column('code', types.String(100))
    name     = Column('name', types.String(200))

    expected = Column('expected', types.Float)
    pledged  = Column('pledged', types.Float)
    pledgedFract  = Column('pledged_fract', types.Float)
    accepted   = Column('accepted', types.Float)
    done       = Column('done', types.Float)
    doneFract  = Column('done_fract', types.Float)
    authors    = Column('authors', types.Float)
    actAuthors = Column( 'phys_users', types.Float )

    sumEprDue  = Column('sum_epr_due', types.Float)

    shiftsPld   = Column('shifts_pld', types.Float)
    eprPledged  = Column('epr_pledged', types.Float)
    eprPldFract = Column('epr_pld_fract', types.Float)
    shiftsDone  = Column('shifts_done', types.Float)
    eprAccounted = Column('epr_accounted', types.Float)
    eprAccFract  = Column('epr_acc_fract', types.Float)

    year = Column(types.Integer)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, code, name, summary, year):

        self.code     = code
        self.name     = name
        self.expected = summary['Expected']
        self.pledged  = summary['Pledged']
        self.accepted = summary['Accepted']
        self.done     = summary['Done']
        self.authors  = summary['Authors']

        self.shiftsPld  = summary['ShiftsPld']
        self.eprPledged = summary['EPRpledged']
        self.shiftsDone = summary['ShiftsDone']
        self.eprAccounted = summary['EPRaccounted']

        self.actAuthors = summary['actAuthors']
        self.sumEprDue  = summary['sumEprDue']

        self.year = year

        self.pledgedFract = 0
        self.doneFract = 0
        self.eprPldFract = 0
        self.eprAccFract = 0
        if float( self.expected + self.sumEprDue ) > 0:
            self.pledgedFract = float(self.pledged)/float(self.expected + self.sumEprDue )
            self.doneFract    = float(self.done)/float(self.expected + self.sumEprDue )
            self.eprPldFract  = float(self.eprPledged)/float(self.expected + self.sumEprDue )
            self.eprAccFract  = float(self.eprAccounted)/float(self.expected + self.sumEprDue )
        #-2017-01-10: as discussed with Kerstin:
        # if an institute has no due's, they have automatically done 100% of their work
        elif float( self.expected + self.sumEprDue ) == 0:
            self.pledgedFract = 1.
            self.doneFract    = 1.
            self.eprPldFract  = 1.
            self.eprAccFract  = 1.

        self.timestamp = datetime.utcnow()

    def to_json(self):
        json_data = {
            'check' : '',
            'code' : self.code ,
            'name' : self.name,
            'Expected' : self.expected,
            'Pledged' : self.pledged,
            'PledgedFract' : self.pledgedFract,
            'Accepted' : self.accepted,
            'Done' : self.done,
            'DoneFract' : self.doneFract,
            'Tasks' : [],
            'Authors' : self.authors,
            'actAuthors' : self.actAuthors,
            'ShiftsPld' : self.shiftsPld,
            'EPRpledged' : self.eprPledged,
            'EPRpldFrac' : self.eprPldFract,
            'ShiftsDone' : self.shiftsDone,
            'EPRaccounted' : self.eprAccounted,
            'EPRaccFrac' : self.eprAccFract,
            'sumEprDue' : self.sumEprDue,
            'lastUpdate' : self.timestamp.strftime('%Y-%m-%dT%H%M%S %z'),
        }
        return json_data

    def __repr__(self):
        msg = '<shift id %s ' % str(self.id)
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % ( k, str(v) )
        msg += '>'
        return msg



class EprDue(EprBaseMixin, DeclarativeBase):
    __tablename__ = 'epr_due'

    id = Column(types.Integer, schema.Sequence('id_seq_eprdue'), primary_key=True)

    userId = Column('user_id', types.Integer, ForeignKey(EprUser.id))
    instId = Column('inst_id', types.Integer, ForeignKey(EprInstitute.id))

    workDue = Column('work_due', types.Float)
    year = Column(types.Integer)

    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, userId, instId, workDue, year):
        self.userId = userId
        self.instId = instId
        self.workDue = workDue
        self.year = year
        self.timestamp = datetime.utcnow()

    def __repr__(self):
        msg = '<eprDue id %s ' % str(self.id)
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % (k, str(v))
        msg += ' timestamp %s ' % (str(self.timestamp),)
        msg += ' >'
        return msg

    def to_json(self):
        json_data = {
            'userId': self.userId,
            'instId': self.instId,
            'workDue': self.workDue,
            'year': self.year,
            'lastUpdate': self.timestamp.strftime('%Y-%m-%dT%H%M%S %z'),
        }
        return json_data


class TimeLineInst(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'time_line_inst'

    id = Column(types.Integer, schema.Sequence('id_seq_timlininst'), primary_key=True)

    # provide information on the "properties" of an institute which can change from year to year:

    code = Column('inst_code', types.String(80), ForeignKey(EprInstitute.code))
    year = Column(types.Integer)

    cmsStatus = Column('cms_status', types.String(40), nullable=False)
    comment = Column(types.String(1000))

    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def to_json(self):
        jsonData = {'id': self.id,
                    'code': self.code,
                    'comment': self.comment,
                    'year': self.year,
                    'cmsStatus': self.cmsStatus,
                    'timestamp': self.timestamp,
                    }
        return jsonData

    def __repr__(self):
        msg = '<TimeLineInst: id %s ' % str(self.id)
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % (k, str(v))
        msg += '>'
        return msg


class TimeLineUser(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'time_line_user'

    id = Column(types.Integer, schema.Sequence('id_seq_timlinuser'), primary_key=True)

    # provide information on the "properties" of an user which can change from year to year (or even more frequently)

    cmsId = Column('user_cms_id', types.Integer, ForeignKey(EprUser.cmsId))
    instCode = Column('inst_code', types.String(80), ForeignKey(EprInstitute.code))
    year = Column('year', types.Integer)

    mainProj = Column('main_project', types.String(80))

    isAuthor = Column("is_author", types.Boolean)
    isSuspended = Column('is_suspended', types.Boolean)
    status = Column('status', types.String(40))
    category = Column("category", types.Integer, ForeignKey(Category.id))
    category_rel = relationship(Category, lazy='joined')

    dueApplicant = Column('due_applicant', types.Float)  # "work due" as a newcomer to be allowed as author
    dueAuthor = Column('due_author', types.Float)  # "work due" to the institute from this author

    # as a user can change his/her affiliation in the course of the year, we track for how long the relevant entries are valid:
    yearFraction = Column('year_fraction', types.Float, default=1.)

    comment = Column(types.String(1000))

    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def to_json(self):
        jsonData = {'id': self.id,
                    'cmsId': self.cmsId,
                    'instCode': self.instCode,
                    'year': self.year,
                    'isAuthor': self.isAuthor,
                    'isSuspended ': self.isSuspended,
                    'status': self.status,
                    'category': self.category,
                    'dueApplicant': self.dueApplicant,
                    'dueAuthor': self.dueAuthor,
                    'yearFraction': self.yearFraction,
                    'comment': self.comment,
                    'timestamp': self.timestamp,
                    }
        return jsonData

    def __repr__(self):
        msg = '<TimeLineUser: id %s ' % str(self.id)
        for k, v in self.to_json().items():
            msg += ' %s:%s ' % (k, str(v))
        msg += '>'
        return msg


class ShiftTaskMap(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'shift_task_map'

    id = Column(types.Integer, schema.Sequence('id_seq_stm'), primary_key=True)

    subSystem = Column('sub_system', types.String(100))
    shiftType = Column('shift_type', types.String(100))
    shiftTypeId = Column('shift_type_id', types.Integer)
    flavourName = Column('flavour_name', types.String(20))

    taskCode = Column('task_code', types.String(80), ForeignKey(Task.code))

    status = Column(types.String(100))
    weight = Column(types.Float)
    addedValueCredits = Column('added_value_cred', types.Float)

    year = Column(types.Integer)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, subSys, sType, sTypeId, flavourName, taskCode, status='ACTIVE', weight=1., addedValueCredits=0, year=2016):
        self.subSystem = subSys
        self.shiftType = sType
        self.shiftTypeId = sTypeId
        self.flavourName = flavourName
        self.taskCode = taskCode
        self.status = status
        self.weight = weight
        self.addedValueCredits = addedValueCredits
        self.year = year
        self.timestamp = datetime.utcnow()

    def to_json(self):
        json_data = {
                'subSystem' : self.subSystem,
                'shiftType' : self.shiftType,
                'shiftTypeId' : self.shiftTypeId,
                'flavourName' : self.flavourName,

                'taskCode'  : self.taskCode,
                'status'    : self.status,

                'weight'    : self.weight,
                'addedValueCredits' : self.addedValueCredits,

                'year'      : self.year,
                'timestamp' : self.timestamp,
        }
        return json_data


class EprCeiling(VersionedMixin, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'ceilings'

    id = Column(types.Integer, schema.Sequence('id_seq_ceilings'), primary_key=True)
    code = Column(types.String(80), unique=True)
    year = Column(types.Integer)
    ceiling = Column(types.Float)
    timestamp = Column(types.DateTime, default=datetime.utcnow)

    def __init__(self, code=None, year=2015, ceiling=0.):
        self.code = code
        self.year = year
        self.ceiling = ceiling
        self.timestamp = datetime.utcnow()

    def __repr__(self):
        return '<ceiling id %i code %s for year %i ceiling %8.2f >' % (self.id, self.code, self.year, self.ceiling)

    def to_json(self):
        json_data = {
            'code': self.code,
            'year': self.year,
            'ceiling': self.ceiling,
            'timestamp': self.timestamp
        }
        return json_data



class ContinuumTransaction(transaction.TransactionBase, EprBaseMixin, DeclarativeBase):
    __tablename__ = 'transaction'
    """
    Continuum struggles with finding its own dynamically created transaction class when binds are brought into the play.
    We are going to help it by swapping versioning_manager.transaction_cls with our own implementation.
    """
    id = Column(types.BigInteger, schema.Sequence('transaction_id_seq', schema=epr_schema_name()), primary_key=True, autoincrement=True)
    remote_addr = Column(types.String(50))
    user_id = Column(EprUser.id.type, ForeignKey(EprUser.id), index=True)
    user = orm.relationship(EprUser)

