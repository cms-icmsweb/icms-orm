#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy import Enum, Float
from icms_orm import cms_common_bind_key, cms_common_schema_name, PseudoEnum
from sqlalchemy.types import Integer, String, Text, DateTime, Boolean, Date, SmallInteger, LargeBinary
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB
import json
import sqlalchemy as sa
from datetime import datetime
import hashlib
from icms_orm.common.common_schema_base import CommonBaseMixin, DeclarativeBase

class Region(CommonBaseMixin, DeclarativeBase):
    code = Column(String(32), primary_key=True)
    name = Column(String(128), nullable=False, unique=True)

class Country(CommonBaseMixin, DeclarativeBase):
    code = Column(String(2), primary_key=True)
    name = Column(String(128), nullable=False, unique=True)
    region_code = Column(String(32), ForeignKey(
        Region.code, onupdate='CASCADE', ondelete='SET NULL'))

class FundingAgency(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    country_code = Column(String(2), ForeignKey(
        Country.code, onupdate='CASCADE', ondelete='CASCADE'))
    name = Column(String(64))


class GenderValues(PseudoEnum):
    FEMALE = 'FEMALE'
    MALE = 'MALE'


gender_enum = Enum(*GenderValues.values(), name='gender_enum',
                   schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)


class Person(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(128), unique=False, nullable=True)
    hr_id = Column(Integer, unique=False, nullable=True)
    cms_id = Column(Integer, unique=True, nullable=False)
    first_name = Column(String(128), nullable=False)
    last_name = Column(String(128), nullable=False)
    email = Column(String(128))
    gender = Column(gender_enum, nullable=True)
    nationality = Column(String(2), ForeignKey(
        Country.code, ondelete='SET NULL', onupdate='CASCADE'), nullable=True)
    date_of_birth = Column(Date)


class PersonStatusValues(PseudoEnum):
    EMPTY = ''
    CMS = 'CMS'
    EXMEMBER = 'EXMEMBER'
    NOSHOW = 'NOSHOW'
    NOTCMS = 'NOTCMS'
    NOTNAME = 'NOTNAME'
    RELATED = 'RELATED'
    CMSEXTENDED = 'CMSEXTENDED'
    CMSEMERITUS = 'CMSEMERITUS'
    CMSVO = 'CMSVO'
    CMSASSOC = 'CMSASSOC'
    DECEASED = 'DECEASED'
    CMSAFFILIATE = 'CMSAFFILIATE'


person_status_enum = Enum(*PersonStatusValues.values(), name='person_status_enum',
                          schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)


class CmsActivityValues(PseudoEnum):
    ADMINISTRATIVE = 'Administrative'
    PHD_STUDENT = 'Doctoral Student'
    ENGINEER = 'Engineer'
    ELECTRONIC_ENGINEER = 'Engineer Electronics'
    MECHANICAL_ENGINEER = 'Engineer Mechanical'
    SOFTWARE_ENGINEER = 'Engineer Software'
    NON_PHD_STUDENT = 'Non-Doctoral Student'
    OTHER = 'Other'
    PHYSICIST = 'Physicist'
    TECHNICIAN = 'Technician'
    THEORETICAL_PHYSICIST = 'Theoretical Physicist'


person_activity_enum = Enum(*CmsActivityValues.values(),
                            name='person_activity_enum', schema=cms_common_schema_name())


class PersonStatus(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(
        Person.cms_id, ondelete='cascade', onupdate='cascade'), nullable=False)
    status = Column(person_status_enum, nullable=True)
    activity = Column(person_activity_enum, nullable=True)
    author_block = Column(Boolean, default=True, nullable=True)
    epr_suspension = Column(Boolean, default=False, nullable=True)
    is_author = Column(Boolean, default=False, nullable=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class Institute(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String(32), unique=True, nullable=False)
    enum_code = Column(String(16), unique=True, nullable=True)
    name = Column(String(256), nullable=False)
    official_joining_date = Column(Date, nullable=True)
    country_code = Column(String(2), ForeignKey(
        Country.code, onupdate='CASCADE', ondelete='CASCADE'))
    fa_id = Column(Integer, ForeignKey(FundingAgency.id,
                                       onupdate='CASCADE', ondelete='SET NULL'))


__inst_status_values = 'Yes,ForReference,No,Cooperating,Associated,Leaving'.split(
    ',')
inst_status_enum = Enum(*__inst_status_values,
                        name='inst_status_enum', schema=cms_common_schema_name())


class InstituteStatus(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String(32), ForeignKey(Institute.code,
                                         ondelete='cascade', onupdate='cascade'), nullable=False)
    status = Column(inst_status_enum, nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class Affiliation(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(
        Person.cms_id, ondelete='cascade', onupdate='cascade'), nullable=False)
    inst_code = Column(String(32), ForeignKey(
        Institute.code, ondelete='cascade', onupdate='cascade'), nullable=False)
    is_primary = Column(Boolean, nullable=False, default=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class InstituteLeader(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(
        Person.cms_id, ondelete='cascade', onupdate='cascade'), nullable=False)
    inst_code = Column(String(32), ForeignKey(
        Institute.code, ondelete='cascade', onupdate='cascade'), nullable=False)
    is_primary = Column(Boolean, nullable=False, default=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class MoStatusValues(PseudoEnum):
    PROPOSED = 'PROPOSED'
    APPROVED = 'APPROVED'
    REJECTED = 'REJECTED'
    APPROVED_LATE = 'APPROVED_LATE'
    # A controversial one - needed to copy data over from the old system, but too generic to be used further
    APPROVED_FREE_GENERIC = 'APPROVED_FREE_GENERIC'
    APPROVED_FREE_LATECOMER = 'APPROVED_FREE_LATECOMER'
    APPROVED_FREE_NEW_INST = 'APPROVED_FREE_NEW_INST'
    APPROVED_FREE_PHD_GRAD = 'APPROVED_FREE_PHD_GRAD'
    APPROVED_FREE_EXTENDED_RIGHTS = 'APPROVED_FREE_EXTENDED_RIGHTS'
    APPROVED_FREE_TOTEM = 'APPROVED_FREE_TOTEM'
    APPROVED_SWAPPED_IN = 'APPROVED_SWAPPED_IN'
    CANCELLED = 'CANCELLED'
    SWAPPED_OUT = 'SWAPPED_OUT'

    @classmethod
    def all_free(cls):
        return [_v for _v in cls.values() if 'APPROVED_FREE' in _v]


mo_status_enum = Enum(*MoStatusValues.values(), name='mo_status_enum',
                      schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)


class MO(CommonBaseMixin, DeclarativeBase):
    __tablename__ = 'mo'

    id = Column(Integer, primary_key=True, autoincrement=True)
    year = Column(SmallInteger, nullable=False)
    cms_id = Column(Integer, ForeignKey(
        Person.cms_id, onupdate='cascade', ondelete='cascade'), nullable=False)
    id = Column(Integer, primary_key=True, autoincrement=True)
    timestamp = Column(DateTime, nullable=False,
                       default=datetime.utcnow, onupdate=datetime.utcnow)
    inst_code = Column(String(32), ForeignKey(
        Institute.code, onupdate='cascade'), nullable=True)
    fa_id = Column(Integer, ForeignKey(
        FundingAgency.id, onupdate='cascade'), nullable=True)
    status = Column(mo_status_enum, nullable=False)
    set_by = Column(Integer, ForeignKey(Person.cms_id), nullable=True)

    @classmethod
    def query_for_latest(cls, cms_id=None, year=None, status_pool=None, initial_query=None):
        """
        Uses something like: select * from (select cms_id, year, rank() over(partition by cms_id, year order by
            timestamp desc, id desc) from mo) f where rank = 1;
        :param cms_id: the cms id to filter on, otherwise all cms_ids go in
        :param year: the year value to filter on, otherwise all years go in
        :param statuses_pool: only look among the entries with one of provided statuses
        :param initial_query: defaults to session.query(MO) but something else can be passed in to facilitate joins
        :return: resulting query
        """
        ssn = MO.session()

        _sq = ssn.query(MO.id.label('id'), sa.func.rank().over(
            order_by=[sa.desc(MO.timestamp), sa.desc(MO.id)], partition_by=[MO.cms_id, MO.year]
        ).label('rnk'))
        if year:
            _sq = _sq.filter(MO.year == year)
        if cms_id:
            _sq = _sq.filter(MO.cms_id == cms_id)
        if status_pool:
            _sq = _sq.filter(sa.or_(*[MO.status == _s for _s in status_pool]))
        _sq = _sq.subquery()

        _q = initial_query or ssn.query(MO)
        _q = _q.join(_sq, sa.and_(_sq.c.id == MO.id, _sq.c.rnk == 1))
        _q = _q.order_by(MO.year, MO.cms_id)
        return _q


class PrehistoryTimeline(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(Person.cms_id),
                    index=True, nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    status_cms = Column(String(64))
    inst_code = Column(String(32), ForeignKey(Institute.code))
    activity_cms = Column(String(64))

    def __init__(self, cms_id, start_date, status_cms, inst_code, activity_cms):
        self.cms_id = cms_id
        self.start_date = start_date
        self.status_cms = status_cms
        self.inst_code = inst_code
        self.activity_cms = activity_cms

    @classmethod
    def copy(cls, other):
        return cls(
            cms_id=other.cms_id,
            start_date=other.start_date,
            status_cms=other.status_cms, inst_code=other.inst_code, activity_cms=other.activity_cms)


class ManagerHistory(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(Person.cms_id), nullable=False)
    inst_code = Column(String(64), ForeignKey(Institute.code), nullable=False)
    year = Column(Integer, nullable=False)
    username = Column(String(64), nullable=False)
    level = Column(SmallInteger, nullable=True)
    role = Column(String(200), nullable=False)
    name = Column(String(200), nullable=False)
    timestamp = Column(DateTime, default=datetime.utcnow, nullable=False)

    def __init__(self, username=None, role=None, name=None, cmsId=None, instCode=None, year=None, timestamp=None,
                 level=None):
        self.cms_id = cmsId
        self.inst_code = instCode
        self.username = username
        self.role = role
        self.name = name
        self.year = year
        self.level = level
        if timestamp:
            self.timestamp = timestamp


class UserAuthorData(CommonBaseMixin, DeclarativeBase):
    """
    Class to host the user's author data, like InspireID, OrcId, ...
    """
    id = Column(Integer, primary_key=True, autoincrement=True)
    hrid = Column(Integer, nullable=False, unique=True)
    cmsid = Column(Integer, nullable=False, unique=True)

    inspireid = Column(String)
    orcid = Column(String)

    lastupdate = Column(DateTime, default=datetime.utcnow, nullable=False)

    def __init__(self, hrid=None, cmsid=None, inspireid=None, orcid=None, lastupdate=None):
        self.hrid = hrid
        self.cmsid = cmsid

        self.inspireid = inspireid
        self.orcid = orcid

        if lastupdate:
            self.lastupdate = lastupdate

    def to_json(self):
        return json.dumps({'id': self.id,
                           'hrId': self.hrid,
                           'cmsId': self.cmsid,
                           'InspireID': self.inspireid,
                           'OrcId': self.orcid,
                           'lastUpdate': str(self.lastupdate),
                           })


class LegacyFlag(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    flag_code = Column(String(32), nullable=False)
    cms_id = Column(Integer, ForeignKey(Person.cms_id), nullable=False)
    remote_id = Column(Integer, nullable=False, unique=True)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class EprAggregateUnitValues(PseudoEnum):
    PLEDGES_DONE = 'PLEDGES DONE'
    SHIFTS_DONE = 'SHIFTS DONE'
    INST_PLEDGES_DONE = 'INST PLEDGES DONE'


epr_aggregate_units_enum = Enum(*EprAggregateUnitValues.values(), name='epr_aggregate_units_enum',
                                schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)


class EprAggregate(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(Person.cms_id), nullable=True)
    inst_code = Column(String(32), ForeignKey(Institute.code), nullable=False)
    project_code = Column(String(32), nullable=False)
    year = Column(Integer, nullable=False)
    aggregate_value = Column(Float, nullable=False, server_default='0')
    aggregate_unit = Column(epr_aggregate_units_enum, nullable=False)

    @property
    def coordinates(self):
        return self.year, self.cms_id, self.inst_code, self.project_code, self.aggregate_unit

    @property
    def fact(self):
        return self.aggregate_value


EprAggregate.add_unique_key('epr_aggregate_coordinates', EprAggregate.cms_id, EprAggregate.inst_code,
                            EprAggregate.project_code, EprAggregate.year, EprAggregate.aggregate_unit)


class CareerEventTypeValues(PseudoEnum):
    PHD_START = 'PHD START'.lower()
    PHD_COMPLETION = 'PHD COMPLETION'.lower()
    CMS_ARRIVAL = 'CMS ARRIVAL'.lower()
    CMS_DEPARTURE = 'CMS DEPARTURE'.lower()


career_event_type_enum = Enum(*CareerEventTypeValues.values(), name='career_event_type_enum',
                              schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)


class CareerEvent(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(Person.cms_id), nullable=False)
    date = Column(Date, nullable=False)
    event_type = Column(career_event_type_enum, nullable=False)


class Project(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String(32), nullable=False, unique=True)
    name = Column(String(64), nullable=False, unique=False)


class ProjectLifespan(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String(32), ForeignKey(Project.code), nullable=False)
    start_year = Column(Integer, nullable=False)
    end_year = Column(Integer, nullable=True)


class Assignment(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    cms_id = Column(Integer, ForeignKey(Person.cms_id), nullable=False)
    project_code = Column(String(32), ForeignKey(Project.code), nullable=False)
    fraction = Column(Float, nullable=False, default=.0)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
