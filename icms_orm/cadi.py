#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from datetime import datetime
from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase

from sqlalchemy import Column, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import String, Integer, Date, Enum, Float, Text, SmallInteger, Boolean, DateTime
from sqlalchemy.dialects.postgresql import JSON
from icms_orm import newcadi_bind_key, newcadi_schema_name, IcmsDeclarativeBasesFactory


DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(newcadi_bind_key())

class CadiNBaseMixin():
    __bind_key__ = newcadi_bind_key()
    __table_args__ = {'schema': newcadi_schema_name()}


rev_com_types = ('ARC',
                 'IRC',
                 )
rev_com_type_enum = Enum(*rev_com_types, name='rev_com_types', schema=newcadi_schema_name(), metadata=DeclarativeBase.metadata)

# -toDo: clarify roles for ARC and IRC
# -toDo:  (can they be overlapping? Are there separate roles for IRC? Can IRC be simply a generic one with role "Institute" ??)
mem_role_types = ('Member',
                  'Chair',
                  'LE',
                  )
mem_role_type_enum = Enum(*rev_com_types, name='mem_role_types',
                          schema=newcadi_schema_name(), metadata=DeclarativeBase.metadata)


# A given review committee with a concrete type (ARC/IRC) and the CADI line it belongs to.
# The type determines the number of members needed (e.g. in 2018: ARC: 4, IRC: 6)
# this should be stored in the DB for increased flexibility
class ReviewCommittee(CadiNBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)

    rc_type = Column(rev_com_type_enum)

    members_needed = Column(Integer, nullable=False)  # -toDo: clarify: e.g.: ARC:4, IRC: 5/6??
    cadi_line = Column(String(100), nullable=False)  # ForeignKey ?

    members = relationship('ReviewCommitteeMember')

    # -?? creator = relationship( 'Person' )
    creator_cms_id = Column(Integer, nullable=False)  # ForeignKey ?

    comment = Column(String(1000))

    # -toDo: clarify statuses for RevCom (both for ARC and IRC - can they be the same ?) - and make that an enum
    # -toDo: can we simply use: [ 'Proposed', 'Waiting', 'Accepted', 'Rejected', 'Active', 'Finished']
    #       instead of       : [ Proposed, Accepted, IN WORK, Waiting, Finished, Aborted(?) ]
    status = Column(String(100))

    # the date when the ARC has been completed/accepted.
    start = Column(DateTime, default=datetime.utcnow)
    end = Column(DateTime, nullable=True, default=None)

    last_update = Column(DateTime, default=datetime.utcnow)
    updater_cms_id = Column(Integer, nullable=False)  # ForeignKey ?

    annex = Column(JSON, nullable=True)

    # may contain: additional info like for AWGs:
    # twiki_url
    # hn_url
    # next_an_id # next ID in this AN (e.g. '11-003')

    def isComplete():
        return ((members_needed > 0) and
                (len([x for x in members if x.status == 'accepted']) >= members_needed))

    def __init__(self, rc_type, members_needed, cadi_line, members, creator_cms_id,
                 comment, status, updater_cms_id,
                 start=datetime.utcnow(),
                 end=None,
                 last_update=datetime.utcnow(),
                 annex=[],
                 ):
        self.rc_type = rc_type
        self.members_needed = members_needed
        self.cadi_line = cadi_line
        self.members = members
        self.creator_cms_id = creator_cms_id
        self.comment = comment
        self.status = status
        self.start = start
        self.end = end
        self.last_update = last_update
        self.updater_cms_id = updater_cms_id
        self.annex = annex

    # instead of to_json() use to_dict() (via our extended DeclarativeBase)


# A member of a specific review committee.
# The entry points back to the review committee for which this is active
# so we can get the type and see if we have a "chair" or a "responsible/contact"
class ReviewCommitteeMember(CadiNBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)

    rc_id = Column(Integer, ForeignKey(ReviewCommittee.id))  # id of review committee

    cms_id = Column(Integer, nullable=True)  # ForeignKey(stub) # member for ARC; representative for Inst if IRC
    inst_code = Column(String(64), nullable=True)  # ForeignKey(stub) # null for ARC; Inst for IRC

    role = Column(mem_role_type_enum)

    # -toDo: clarify status (see RevCom above): [ 'proposed', 'waiting', 'accepted', 'reject', 'active', 'done/finished'(?for IRC?), 'New Entry' ?db? ]
    status = Column(String(128), nullable=False)

    comment = Column(String(1000))

    start = Column(DateTime, default=datetime.utcnow)
    end = Column(DateTime, nullable=True, default=None)

    last_update = Column(DateTime, default=datetime.utcnow)  # enough to have this and keep all status changes ?

    def is_chair():
        return 'chair' in role.lower()

    def __init__(self, rc_id, cms_id, inst_code, role,
                 comment, status,
                 start=datetime.utcnow(),
                 end=None,
                 last_update=datetime.utcnow(),
                 ):
        self.rc_id = rc_id
        self.cms_id = cms_id
        self.inst_code = inst_code
        self.role = role
        self.comment = comment
        self.status = status
        self.start = start
        self.end = end
        self.last_update = last_update

    # instead of to_json() use to_dict() (via our extended DeclarativeBase)

# AWG will be separate, using eGroups for authorisation/member
