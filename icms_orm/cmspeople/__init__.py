"""
Decorates some DB objects with relationships that could not be defined in their respective source files due to
circular dependencies
"""
from .common import PeopleBaseMixin, DeclarativeBase
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import and_
from sqlalchemy.orm import relationship, join
from .institutes import *
from .people import *
from .management import *
from .projects import *


Institute.cbiCmsId = Column('CBIid', SmallInteger, ForeignKey(Person.cmsId), nullable=True)
Institute.cbiPerson = relationship(Person, foreign_keys=[Institute.cbiCmsId])
Institute.cbdCmsId = Column('CBDid', SmallInteger, ForeignKey(Person.cmsId), nullable=True)
Institute.cbdPerson = relationship(Person, foreign_keys=[Institute.cbdCmsId])
Institute.cbd2CmsId = Column('CBD2id', SmallInteger, ForeignKey(Person.cmsId), nullable=True)
Institute.cbd2Person = relationship(Person, foreign_keys=[Institute.cbd2CmsId])
Institute.delegateCmsId = Column('DelegateId', SmallInteger, ForeignKey(Person.cmsId))
Institute.delegatePerson = relationship(Person, foreign_keys=[Institute.delegateCmsId])

Person.instCode = Column('InstCode', String(40), ForeignKey(Institute.code))
Person.institute = relationship(Institute, backref='members', foreign_keys=[Person.instCode], lazy='joined')
Person.instCodeNow = Column('InstCodeNow', String(100), ForeignKey(Institute.code))
Person.instituteNow = relationship(Institute, foreign_keys=[Person.instCodeNow])
Person.instCodeOther = Column('InstCodeOther', String(100), ForeignKey(Institute.code))
Person.instituteOther = relationship(Institute, foreign_keys=[Person.instCodeOther])

Person.flags = relationship(Flag,
                            secondary=join(PeopleFlagsAssociation, Flag, PeopleFlagsAssociation.flagId==Flag.id),
                            primaryjoin=and_(Person.cmsId == PeopleFlagsAssociation.cmsId), back_populates=Flag.people, lazy='select')

Flag.people = relationship(Person, secondary=join(PeopleFlagsAssociation, Person, PeopleFlagsAssociation.cmsId==Person.cmsId),
                            primaryjoin=and_(Flag.id == PeopleFlagsAssociation.flagId), lazy='subquery')

