from typing import Optional
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.types import (
    Float,
    SmallInteger,
    Integer,
    Enum,
    DateTime,
    Date,
    Text,
    String,
)
from type_decorators import StringBool, StringInt
from datetime import datetime, date
import re
from icms_orm.cmspeople.common import PeopleBaseMixin, DeclarativeBase
from icms_orm.cmspeople.institutes import Institute
from icms_orm import PseudoEnum
from icms_orm import translate_bool, translate_type


class MemberStatusValues(PseudoEnum):
    PERSON_STATUS_CMS = "CMS"
    PERSON_STATUS_EXMEMBER = "EXMEMBER"
    PERSON_STATUS_NOSHOW = "NOSHOW"
    PERSON_STATUS_NOTCMS = "NOTCMS"
    PERSON_STATUS_NOTNAME = "NOTNAME"
    PERSON_STATUS_RELATED = "RELATED"
    PERSON_STATUS_CMSEXTENDED = "CMSEXTENDED"
    PERSON_STATUS_CMSEMERITUS = "CMSEMERITUS"
    PERSON_STATUS_CMSVO = "CMSVO"
    PERSON_STATUS_CMSASSOC = "CMSASSOC"
    PERSON_STATUS_DECEASED = "DECEASED"
    PERSON_STATUS_CMSAFFILIATE = "CMSAFFILIATE"


# legacy: enum values exposed directly for pre-existing imports
PERSON_STATUS_CMS = MemberStatusValues.PERSON_STATUS_CMS
PERSON_STATUS_EXMEMBER = MemberStatusValues.PERSON_STATUS_EXMEMBER
PERSON_STATUS_NOSHOW = MemberStatusValues.PERSON_STATUS_NOSHOW
PERSON_STATUS_NOTCMS = MemberStatusValues.PERSON_STATUS_NOTCMS
PERSON_STATUS_NOTNAME = MemberStatusValues.PERSON_STATUS_NOTNAME
PERSON_STATUS_RELATED = MemberStatusValues.PERSON_STATUS_RELATED
PERSON_STATUS_CMSEXTENDED = MemberStatusValues.PERSON_STATUS_CMSEXTENDED
PERSON_STATUS_CMSEMERITUS = MemberStatusValues.PERSON_STATUS_CMSEMERITUS
PERSON_STATUS_CMSVO = MemberStatusValues.PERSON_STATUS_CMSVO
PERSON_STATUS_CMSASSOC = MemberStatusValues.PERSON_STATUS_CMSASSOC
PERSON_STATUS_DECEASED = MemberStatusValues.PERSON_STATUS_DECEASED
PERSON_STATUS_CMSAFFILIATE = MemberStatusValues.PERSON_STATUS_CMSAFFILIATE


class YesNoValues(PseudoEnum):
    YES = "YES"
    NO = "NO"


yes_no_enum = Enum(
    *YesNoValues.values(), name="yesno", metadata=DeclarativeBase.metadata
)
member_status_enum = Enum(
    *MemberStatusValues.values(),
    name="member_status",
    metadata=DeclarativeBase.metadata
)
email_issue_enum = Enum(
    "DONOTSEND",
    "NOEMAIL",
    "BADEMAIL",
    name="email_issue",
    metadata=DeclarativeBase.metadata,
)
photo_public_enum = Enum(
    "never",
    "public",
    "restricted",
    name="photo_public",
    metadata=DeclarativeBase.metadata,
)


class Flag(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleFlagsName"

    id = Column("PeopleFlgId", String(30), primary_key=True)
    desc = Column("PeopleFlgName", String(60))
    people = None

    def __unicode__(self):
        return "Flag %s (%s)" % (self.id, self.desc)


class CountryRegionMapping(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "CountryRegionMapping"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column("name", String(255))
    name2 = Column("name2", String(255))
    code = Column("code", String(100))
    region_code = Column("region_code", String(100))
    region_name = Column("region_name", String(255))

    def __unicode__(self):
        return "CountryRegionMapping %s (%s)" % (self.name, self.region_name)


class MemberActivity(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleActivityName"

    id = Column("Activity", SmallInteger, primary_key=True, autoincrement=True)
    name = Column("ActivName", String(40), nullable=False)

    def __unicode__(self):
        return "%s[#%d]" % (self.name, self.id)

    @staticmethod
    def does_activity_name_trigger_application(activity_name):
        return activity_name and activity_name.lower().strip() in {
            "doctoral student",
            "physicist",
        }

    @staticmethod
    def does_activity_name_allow_signing(activity_name):
        return (
            MemberActivity.does_activity_name_trigger_application(activity_name)
            or "engineer" in activity_name.lower()
        )

    @staticmethod
    def get_authorship_eligible_activities(db_session=None):
        db_session = db_session or MemberActivity.session()
        activities = db_session.query(MemberActivity).all()
        return [
            (ma.id, ma.name)
            for ma in activities
            if MemberActivity.does_activity_name_allow_signing(ma.name)
        ]

    @staticmethod
    def get_authorship_eligible_activity_names(db_session=None):
        return [
            a[1] for a in MemberActivity.get_authorship_eligible_activities(db_session)
        ]


class Person(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleData"

    cmsId = Column("CMSid", SmallInteger, primary_key=True, autoincrement=True)
    hrId = Column("HRpersonId", Integer, default=-1)
    activityId = Column("Activity", SmallInteger, ForeignKey(MemberActivity.id))
    activity = relationship(MemberActivity, backref="people", lazy="joined")
    activityTime = Column("ActivityTime", String(10), nullable=True)
    cmsTime = Column("CMSTime", String(10), nullable=True)
    niceLogin = Column("loginniceid", String(100))
    loginId = Column("loginid", String(100), nullable=False)

    project = Column("Projects", String(40), nullable=False, default="")

    firstName = Column("NamfCMS", String(40))
    lastName = Column("NameCMS", String(40))
    status = Column("StatusCMS", member_status_enum)

    dateStart = Column("DateStart", DateTime)
    dateEndSign = Column("DateEndSign", Date)
    birthDate = Column("BirthDate", Date)
    dateCreation = Column("DateCreation", DateTime)
    dateRegistration = Column("DateRegistration", DateTime)

    @property
    def dateDeparture(self) -> Optional[date]:
        return (
            datetime.strptime(self.exDate, "%Y-%m-%d").date() if self.exDate else None
        )

    @dateDeparture.setter
    def dateDeparture(self, value: date):
        self.exDate = value.strftime("%Y-%m-%d")

    datetimeModified = Column("DateModif", DateTime)
    dateAuthorUnsuspension = Column("DateAuthorUnsuspend", Date)
    dateAuthorSuspension = Column("DateAuthorSuspend", Date)
    dateAuthorBlock = Column("DateAuthorBlock", Date)
    dateAuthorUnblock = Column("DateAuthorUnblock", Date)

    isAuthor = Column("Author", StringBool, default=False)
    isAuthorSuspended = Column("AuthorSuspend", StringBool, default=False)
    isAuthorAllowed = Column("AuthorAllow", StringBool, default=False)
    isAuthorBlock = Column("AuthorBlock", StringBool, default=False)
    authorId = Column("AuthorId", String(40), nullable=True)

    dateMailSentBlock = Column("DateMailSentBlock", Date, nullable=True)
    dateMailSentAuthor = Column("DateMailSentAuthor", Date, nullable=True)

    isAuthor2020 = Column("Author_2020", SmallInteger, default=False)
    isAuthor2019 = Column("Author_2019", SmallInteger, default=False)
    isAuthor2018 = Column("Author_2018", SmallInteger, default=False)
    isAuthor2017 = Column("Author_2017", SmallInteger, default=False)
    isAuthor2016 = Column("Author_2016", SmallInteger, default=False)
    isAuthor2015 = Column("Author_2015", SmallInteger, default=False)
    isAuthor2014 = Column("Author_2014", SmallInteger, default=False)
    isAuthor2013 = Column("Author_2013", SmallInteger, default=False)
    isAuthor2012 = Column("Author_2012", SmallInteger, default=False)
    isAuthor2011 = Column("Author_2011", SmallInteger, default=False)
    isAuthor2010 = Column("Author_2010", SmallInteger, default=False)
    isAuthor2009 = Column("Author_2009", SmallInteger, default=False)

    epr_due_2013 = Column("due_2013", Float, default=0.0)
    epr_due_2014 = Column("due_2014", Float, default=0.0)
    epr_due_2015 = Column("due_2015", Float, default=0.0)
    epr_due_2016 = Column("due_2016", Float, default=0.0)
    epr_due_2017 = Column("due_2017", Float, default=0.0)

    emailProblem = Column("EmailProblem", email_issue_enum, default=None, nullable=True)

    priEnabled = Column("PriEnabled", StringBool)
    zhFlag = Column("ZHflag", StringBool)
    physicsAccess = Column("PhysicsAccess", StringBool)

    fundingSpecial = Column("FundingSpecial", String(32), nullable=True, default=None)
    domainActivity = Column("DomainActivity", String(16), nullable=True, default=None)
    infn = Column("INFN", String(4), nullable=True)
    univOther = Column(
        "UNIVOTHER",
        String(4),
    )

    instCodeFrom = Column("InstCodeFrom", String(40))

    nationality = Column("Nationality", String(3))
    nationality2 = Column("Nationality2", String(3))
    photoPublic = Column("PhotoPublic", photo_public_enum)
    exDate = Column("EXYear", String(20))
    remarks = Column("Remarks", String(255))
    remarksSecr = Column("RemarkSecr", String(255))
    physicsAccess = Column("PhysicsAccess", String(1))
    zhFlag = Column("ZHflag", String(1))

    # "FORWARD-DECLARATIONS"
    flags = None
    instCode = None
    institute = None
    instCodeNow = None
    instituteNow = None
    instCodeOther = None
    instituteOther = None

    # UNUSED AS OF YET:
    # Function            = Column(String )
    # contactOfFA         = Column(String )
    # InstFax             = Column(String )
    # NameAmbigNb         = Column(String )
    # NoteAuthor          = Column(String )
    # OfficeCMS           = Column(String )
    # MOstatus            = Column(Enum('notMO','DAQ','DI','EC','GEN','HC','MU','OFF','TK','TRG') )
    # InstCodeSign        = Column(String )
    # AWGexcludeAL        = Column(String )
    # AuthorTK            = Column(String )
    # PriEnabled          = Column(String )
    # AuthorRemarks       = Column(Text )
    # AuthorId            = Column(String )
    # StatusCMS_2014      = Column(Enum('CMS','EXMEMBER','NOSHOW','NOTCMS','NOTNAME','RELATED','CMSEXTENDED','CMSEMERITUS','CMSVO','CMSASSOC','DECEASED','CMSAFFILIATE') )
    # StatusCMS_2015      = Column(Enum('CMS','EXMEMBER','NOSHOW','NOTCMS','NOTNAME','RELATED','CMSEXTENDED','CMSEMERITUS','CMSVO','CMSASSOC','DECEASED','CMSAFFILIATE') )
    # AuthorUpgradeTP     = Column(String )
    # AuthorHiggs         = Column(String )
    # AuthorOpenData      = Column(String )
    # AuthorStats         = Column(Text )

    def get_authorship_application_start_date(self, db_session=None):
        """
        Deprecated, please look at icmscommon.businesslogic.authorship
        :param db_session: no need to provide it as
        :return: a tuple containing a date and a boolean stating whether the person can be a slip-through
        """
        db_session = db_session or Person.session()
        if self.isAuthorSuspended:
            return None, False
        else:
            # assuming all criteria were met right since the creation
            # todo: shouldn't use the starting date here?! - not necessarily. if one leaves, they will have history...
            true_start = self.dateCreation.date()

            # starting from the most recent, look for traces of activity that would fail to meet requirements
            for activity, date in self.history.get_activity_changes():
                if activity not in (
                    x[0]
                    for x in MemberActivity.get_authorship_eligible_activities(
                        db_session
                    )
                ):
                    true_start = date
                    break

            # starting from the most recent, check if a status change can even further narrow down the 'green zone'
            for status, date in self.history.get_status_changes():
                if not status.startswith("CMS") or date < true_start:
                    true_start = max(date, true_start)
                    break

            # A slipthrough is a case when somebody joins, is assigned an irrelevant category and never gets
            # suspended so that as soon as they are promoted (category), they seem as author applicant.

            possible_slipthrough = False
            if (
                (
                    self.dateAuthorUnsuspension is None
                    and true_start > self.dateCreation.date()
                )
                or self.dateAuthorUnsuspension is not None
                and true_start > self.dateAuthorUnsuspension
            ):
                possible_slipthrough = True

        return (
            max(true_start, self.dateAuthorUnsuspension or true_start),
            possible_slipthrough,
        )


class PeopleFlagsAssociation(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleFlagsRel"
    id = Column("PrKeyPF", Integer, primary_key=True, autoincrement=True)
    cmsId = Column("CMSid", SmallInteger, ForeignKey(Person.cmsId))
    flagId = Column("PeopleFlgId", String(30), ForeignKey(Flag.id))
    person = relationship(Person)


class PersonHistory(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleHistory"

    # ?: makes a group non-capturing, *? is for non-greedy
    __activity_pattern = r"(\d+/\d+/\d+)(?:.)*?[-;]Activity(?::|:to(?:\d+)from)(\d+)"
    __status_pattern = (
        r"(\d+/\d+/\d+)(?:.)*?[-;]StatusCMS:(?:to[A-Z]+(?=from)from)?(\w+)"
    )
    # iCMSfor9899_10/11/2016-fromPH:CERNstatus=USERand-StatusCMS=CMSandLocalPercent=50
    __status_pattern_from_ph = r"(\d+/\d+/\d+)-fromPH:(?:.)*StatusCMS=(\w+)and"
    __date_pattern = r"((\d+)/(\d+)/(\d+))"
    __new_history_line_pattern = r"(.*)_(\d+)/(\d+)/(\d+)-"

    cmsId = Column(
        "CMSid",
        SmallInteger,
        ForeignKey(Person.cmsId),
        primary_key=True,
        autoincrement=False,
    )
    person = relationship(Person, backref=backref("history", uselist=False))
    history = Column("History", Text)

    def get_activity_changes(self):
        """
        :return: an array of tuples representing past activities and their terminating dates (activity, date)
         Sorted by date, descending.
        """
        results = []
        for line in self.__get_history_lines():
            # print 'get_activity_changes: %s' % line
            m = re.search(PersonHistory.__activity_pattern, line)
            if m is not None and len(m.groups()) >= 2:
                parsed_date = PersonHistory.__extract_date(m.group(1), full_line=line)
                if parsed_date is None:
                    parsed_date = results[-1][1]
                results.append(
                    (
                        int(m.group(2)),
                        parsed_date if parsed_date else self.person.dateCreation,
                    )
                )
        return results

    def get_status_changes(self):
        """
        :return: an array of tuples representing past statuses and their terminating dates (status, date)
         Sorted by date, descending.
        """
        results = []
        for line in self.__get_history_lines():
            # print 'get_status_changes: %s' % line
            m = re.search(PersonHistory.__status_pattern, line)
            if m is not None and len(m.groups()) >= 2:
                # print 'primary expression: A MATCH'
                parsed_date = PersonHistory.__extract_date(m.group(1), full_line=line)
                if parsed_date is None:
                    parsed_date = results[-1][1]
                results.append(
                    (
                        m.group(2),
                        parsed_date if parsed_date else self.person.dateCreation,
                    )
                )
            else:
                # print 'primary expression: no match'
                # sometimes we get a line that states the current state rather than the past
                # and sometimes it mentions stuff that it doesn't even change
                m = re.search(PersonHistory.__status_pattern_from_ph, line)
                if m is not None and len(m.groups()) >= 2:
                    parsed_date = PersonHistory.__extract_date(
                        m.group(1), full_line=line
                    )
                    if parsed_date is None:
                        parsed_date = results[-1][1]
                    results.append(
                        (
                            "UNKNOWN_to_%s" % m.group(2),
                            parsed_date if parsed_date else self.person.dateCreation,
                        )
                    )
        # try refining the results (due to presence of "fromPH: blabla")
        final_results = []
        for i, v in enumerate(results):
            if "UNKNOWN" not in v[0]:
                final_results.append(v)
            elif (
                i + 1 < len(results) and results[i + 1][0] == "CMS" and "to_CMS" in v[0]
            ):
                final_results.append(v)
        return final_results

    def __get_history_lines(self):
        lines = []
        separator = "\r\n"
        for line in self.history.split(separator):
            if len(line) > 0:
                if re.match(PersonHistory.__new_history_line_pattern, line):
                    lines.append(line)
                elif lines:
                    lines[-1] += " " + line
        return lines

    def get_history_lines(self):
        return self.__get_history_lines()

    @staticmethod
    def __extract_date(date_part, full_line):
        """
        Weird parsing stems from the fact that between 2006/11/17 and 2007/07/27 the dates were saved as YYYY/MM/DD
        """
        if date_part is None:
            return None
        m = re.search(PersonHistory.__date_pattern, date_part)
        if m is not None:
            order = (4, 3, 2)
            if int(m.group(3)) > 12 or (
                m.group(4) == "2007" and int(m.group(3)) <= 7 and int(m.group(2)) <= 7
            ):
                order = (4, 2, 3)

            args = [int(m.group(x)) for x in order]

            if full_line.strip().startswith("www-jpp") and args[0] < 100:
                args[0] += 1900 if args[0] >= 50 else 2000

            return date(*args)


class User(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleUserData"

    cmsId = Column(
        "CMSid",
        SmallInteger,
        ForeignKey(Person.cmsId),
        primary_key=True,
        autoincrement=False,
    )
    activityDetails = Column("ActivityDetails", String(40), nullable=True)
    person = relationship(Person, backref=backref("user", uselist=False))
    email1 = Column("Email1", String(60), nullable=True)
    email2 = Column("Email2", String(60), nullable=True)
    emailCern = Column("EmailCERN", String(60), nullable=True)
    mailWhere = Column(
        "MailWhere",
        Enum("CERN", "institute", name="mail_where", metadata=DeclarativeBase.metadata),
    )
    title = Column(
        "Title",
        Enum(
            "Ms.",
            "Mr.",
            "Dr.",
            "Prof.",
            "Eng.",
            name="person_title",
            metadata=DeclarativeBase.metadata,
        ),
        nullable=True,
    )
    expOther = Column("ExpOther", String(40), nullable=True)
    expOtherPercent = Column("ExpOtherPercent", SmallInteger, nullable=True)
    instAddrSpecial = Column("InstAddrSpecial", String(255), nullable=True)
    instPhone = Column("InstPhone", String(40), nullable=True)
    instPhonePriv = Column("InstPhonePriv", String(40), nullable=True)
    localAddrPriv = Column("LocalAddrPriv", String(255), nullable=True)
    localPercent = Column("LocalPercent", SmallInteger, nullable=True)
    localPhonePriv = Column("LocalPhonePriv", String(40), nullable=True)
    sex = Column(
        "Sex",
        Enum("F", "M", name="gender", metadata=DeclarativeBase.metadata),
        nullable=True,
    )
    wwwPerso = Column("wwwPerso", String(255), nullable=True)
    nameSignature = Column("NameSignature", String(50), nullable=True)

    def get_email(self):
        if (
            self.mailWhere
            and self.mailWhere.strip()
            and self.mailWhere.upper().strip() != "CERN"
        ):
            return self.email1 or self.email2 or self.emailCern
        return self.emailCern or self.email1 or self.email2


class MoData(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleMOData"

    cmsId = Column(
        "CMSId",
        SmallInteger,
        ForeignKey(Person.cmsId),
        primary_key=True,
        autoincrement=False,
    )
    person = relationship(Person, backref=backref("modata", uselist=False))

    mo2006 = Column("MO2006", yes_no_enum, default="NO")
    mo2007 = Column("MO2007", yes_no_enum, default="NO")
    mo2008 = Column("MO2008", yes_no_enum, default="NO")
    mo2009 = Column("MO2009", yes_no_enum, default="NO")
    mo2010 = Column("MO2010", yes_no_enum, default="NO")
    mo2011 = Column("MO2011", yes_no_enum, default="NO")
    mo2012 = Column("MO2012", yes_no_enum, default="NO")
    mo2013 = Column("MO2013", yes_no_enum, default="NO")
    mo2014 = Column("MO2014", yes_no_enum, default="NO")
    mo2015 = Column("MO2015", yes_no_enum, default="NO")
    mo2016 = Column("MO2016", yes_no_enum, default="NO")
    mo2017 = Column("MO2017", yes_no_enum, default="NO")
    mo2018 = Column("MO2018", yes_no_enum, default="NO")
    mo2019 = Column("MO2019", yes_no_enum, default="NO")
    mo2020 = Column("MO2020", yes_no_enum, default="NO")
    mo2021 = Column("MO2021", yes_no_enum, default="NO")
    mo2022 = Column("MO2022", yes_no_enum, default="NO")
    mo2023 = Column("MO2023", yes_no_enum, default="NO")
    mo2024 = Column("MO2024", yes_no_enum, default="NO")
    mo2025 = Column("MO2025", yes_no_enum, default="NO")
    mo2026 = Column("MO2026", yes_no_enum, default="NO")

    phdMo2011 = Column("PHDMO2011", yes_no_enum, default="NO")
    phdMo2012 = Column("PHDMO2012", yes_no_enum, default="NO")
    phdMo2013 = Column("PHDMO2013", yes_no_enum, default="NO")
    phdMo2014 = Column("PHDMO2014", yes_no_enum, default="NO")
    phdMo2015 = Column("PHDMO2015", yes_no_enum, default="NO")
    phdMo2016 = Column("PHDMO2016", yes_no_enum, default="NO")
    phdMo2017 = Column("PHDMO2017", yes_no_enum, default="NO")
    phdMo2018 = Column("PHDMO2018", yes_no_enum, default="NO")
    phdMo2019 = Column("PHDMO2019", yes_no_enum, default="NO")
    phdMo2020 = Column("PHDMO2020", yes_no_enum, default="NO")
    phdMo2021 = Column("PHDMO2021", yes_no_enum, default="NO")
    phdMo2022 = Column("PHDMO2022", yes_no_enum, default="NO")
    phdMo2023 = Column("PHDMO2023", yes_no_enum, default="NO")
    phdMo2024 = Column("PHDMO2024", yes_no_enum, default="NO")
    phdMo2025 = Column("PHDMO2025", yes_no_enum, default="NO")
    phdMo2026 = Column("PHDMO2026", yes_no_enum, default="NO")

    freeMo2006 = Column("FREEMO2006", yes_no_enum, default="NO")
    freeMo2007 = Column("FREEMO2007", yes_no_enum, default="NO")
    freeMo2008 = Column("FREEMO2008", yes_no_enum, default="NO")
    freeMo2009 = Column("FREEMO2009", yes_no_enum, default="NO")
    freeMo2010 = Column("FREEMO2010", yes_no_enum, default="NO")
    freeMo2011 = Column("FREEMO2011", yes_no_enum, default="NO")
    freeMo2012 = Column("FREEMO2012", yes_no_enum, default="NO")
    freeMo2013 = Column("FREEMO2013", yes_no_enum, default="NO")
    freeMo2014 = Column("FREEMO2014", yes_no_enum, default="NO")
    freeMo2015 = Column("FREEMO2015", yes_no_enum, default="NO")
    freeMo2016 = Column("FREEMO2016", yes_no_enum, default="NO")
    freeMo2017 = Column("FREEMO2017", yes_no_enum, default="NO")
    freeMo2018 = Column("FREEMO2018", yes_no_enum, default="NO")
    freeMo2019 = Column("FREEMO2019", yes_no_enum, default="NO")
    freeMo2020 = Column("FREEMO2020", yes_no_enum, default="NO")
    freeMo2021 = Column("FREEMO2021", yes_no_enum, default="NO")
    freeMo2022 = Column("FREEMO2022", yes_no_enum, default="NO")
    freeMo2023 = Column("FREEMO2023", yes_no_enum, default="NO")
    freeMo2024 = Column("FREEMO2024", yes_no_enum, default="NO")
    freeMo2025 = Column("FREEMO2025", yes_no_enum, default="NO")
    freeMo2026 = Column("FREEMO2026", yes_no_enum, default="NO")

    phdInstCode2011 = Column("PHDIC2011", String(40), ForeignKey(Institute.code))
    phdInstCode2012 = Column("PHDIC2012", String(40), ForeignKey(Institute.code))
    phdInstCode2013 = Column("PHDIC2013", String(40), ForeignKey(Institute.code))
    phdInstCode2014 = Column("PHDIC2014", String(40), ForeignKey(Institute.code))
    phdInstCode2015 = Column("PHDIC2015", String(40), ForeignKey(Institute.code))
    phdInstCode2016 = Column("PHDIC2016", String(40), ForeignKey(Institute.code))
    phdInstCode2017 = Column("PHDIC2017", String(40), ForeignKey(Institute.code))
    phdInstCode2018 = Column("PHDIC2018", String(40), ForeignKey(Institute.code))
    phdInstCode2019 = Column("PHDIC2019", String(40), ForeignKey(Institute.code))
    phdInstCode2020 = Column("PHDIC2020", String(40), ForeignKey(Institute.code))
    phdInstCode2021 = Column("PHDIC2021", String(40), ForeignKey(Institute.code))
    phdInstCode2022 = Column("PHDIC2022", String(40), ForeignKey(Institute.code))
    phdInstCode2023 = Column("PHDIC2023", String(40), ForeignKey(Institute.code))
    phdInstCode2024 = Column("PHDIC2024", String(40), ForeignKey(Institute.code))
    phdInstCode2025 = Column("PHDIC2025", String(40), ForeignKey(Institute.code))
    phdInstCode2026 = Column("PHDIC2026", String(40), ForeignKey(Institute.code))

    phdInst2011 = relationship(Institute, foreign_keys=[phdInstCode2011])
    phdInst2012 = relationship(Institute, foreign_keys=[phdInstCode2012])
    phdInst2013 = relationship(Institute, foreign_keys=[phdInstCode2013])
    phdInst2014 = relationship(Institute, foreign_keys=[phdInstCode2014])
    phdInst2015 = relationship(Institute, foreign_keys=[phdInstCode2015])
    phdInst2016 = relationship(Institute, foreign_keys=[phdInstCode2016])
    phdInst2017 = relationship(Institute, foreign_keys=[phdInstCode2017])
    phdInst2019 = relationship(Institute, foreign_keys=[phdInstCode2019])
    phdInst2019 = relationship(Institute, foreign_keys=[phdInstCode2019])
    phdInst2020 = relationship(Institute, foreign_keys=[phdInstCode2020])
    phdInst2021 = relationship(Institute, foreign_keys=[phdInstCode2021])
    phdInst2022 = relationship(Institute, foreign_keys=[phdInstCode2022])
    phdInst2023 = relationship(Institute, foreign_keys=[phdInstCode2022])
    phdInst2024 = relationship(Institute, foreign_keys=[phdInstCode2022])
    phdInst2025 = relationship(Institute, foreign_keys=[phdInstCode2025])
    phdInst2026 = relationship(Institute, foreign_keys=[phdInstCode2026])

    phdFa2011 = Column("PHDFA2011", String(40), nullable=True, default="")
    phdFa2012 = Column("PHDFA2012", String(40), nullable=True, default="")
    phdFa2013 = Column("PHDFA2013", String(40), nullable=True, default="")
    phdFa2014 = Column("PHDFA2014", String(40), nullable=True, default="")
    phdFa2015 = Column("PHDFA2015", String(40), nullable=True, default="")
    phdFa2016 = Column("PHDFA2016", String(40), nullable=True, default="")
    phdFa2017 = Column("PHDFA2017", String(40), nullable=True, default="")
    phdFa2018 = Column("PHDFA2018", String(40), nullable=True, default="")
    phdFa2019 = Column("PHDFA2019", String(40), nullable=True, default="")
    phdFa2020 = Column("PHDFA2020", String(40), nullable=True, default="")
    phdFa2021 = Column("PHDFA2021", String(40), nullable=True, default="")
    phdFa2022 = Column("PHDFA2022", String(40), nullable=True, default="")
    phdFa2023 = Column("PHDFA2023", String(40), nullable=True, default="")
    phdFa2024 = Column("PHDFA2024", String(40), nullable=True, default="")
    phdFa2025 = Column("PHDFA2025", String(40), nullable=True, default="")
    phdFa2026 = Column("PHDFA2026", String(40), nullable=True, default="")

    @classmethod
    def get_columns_for_year(cls, year):
        """
        :param year:
        :return: (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col)
        """
        cols = (
            cls.mo2015,
            cls.phdMo2015,
            cls.freeMo2015,
            cls.phdInstCode2015,
            cls.phdFa2015,
        )
        return tuple(
            [getattr(cls, c.key.replace("2015", str(year)), None) for c in cols]
        )

    @classmethod
    def get_phd_fa_column_for_year(cls, year):
        return getattr(cls, cls.phdFa2015.key.replace("2015", str(year)))

    @classmethod
    def get_mo_column_for_year(cls, year):
        return getattr(cls, cls.mo2015.key.replace("2015", str(year)))

    @classmethod
    def get_phd_mo_column_for_year(cls, year):
        return getattr(cls, cls.phdMo2015.key.replace("2015", str(year)))

    @classmethod
    def get_free_mo_column_for_year(cls, year):
        return getattr(cls, cls.freeMo2015.key.replace("2015", str(year)))


class PersonCernData(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleCERNData"

    cmsId = Column("CMSid", SmallInteger, ForeignKey(Person.cmsId), primary_key=True)
    person = relationship(Person, backref=backref("cernData", uselist=False))
    dateEnd = Column("CERNDateEnd", Date, nullable=True, default=None)
    dateStart = Column("CERNDateStart", Date, nullable=True, default=None)
    div = Column("CERNdiv", String(40), nullable=True, default=None)
    fax = Column("CERNfax", String(40), nullable=True, default=None)
    group = Column("CERNgroup", String(40), nullable=True, default=None)
    gsm = Column("CERNGSM", String(40), nullable=True, default=None)
    cernId = Column("CERNid", String(10), nullable=True, default=None)
    office = Column("CERNoffice", String(40), nullable=True, default=None)
    phone = Column("CERNphone", String(40), nullable=True, default=None)
    postalbox = Column("CERNpostalbox", String(40), nullable=True, default=None)
    section = Column("CERNsection", String(40), nullable=True, default=None)
    status = Column("CERNstatus", String(40), nullable=True, default=None)
    lastName = Column("NameCERN", String(40), nullable=True, default=None)
    firstName = Column("NamfCERN", String(40), nullable=True, default=None)
    hrDiv = Column("HRdiv", String(40), nullable=True, default=None)
    hrGroup = Column("HRgroup", String(40), nullable=True, default=None)
    hrStatus = Column("HRstatus", String(40), nullable=True, default=None)
    hrEndContract = Column("HRendContract", String(40), nullable=True, default=None)
    hrHomeInst = Column("HRhomeInst", String(40), nullable=True, default=None)
    hrSupervisor = Column("HRsupervisor", String(40), nullable=True, default=None)


class Delegation(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "Delegations"

    def __init__(
        self,
        voting,
        fromCmsId,
        toCmsId,
        createdByCmsId=None,
        year=date.today().year,
        creationTime=datetime.now(),
        status="OK",
    ):
        self.voting = voting
        self.fromCmsId = fromCmsId
        self.toCmsId = toCmsId
        self.createdByCmsId = createdByCmsId if createdByCmsId else fromCmsId
        self.year = year
        self.creationTime = creationTime
        self.status = status

    id = Column("PrKeyPF", Integer, primary_key=True)
    year = Column("Year", StringInt(length=4), nullable=False)
    creationTime = Column("DateCreation", DateTime, nullable=False)
    createdByCmsId = Column(
        "createdByCMSid", SmallInteger, ForeignKey(Person.cmsId), nullable=False
    )
    creator = relationship(Person, foreign_keys=[createdByCmsId])
    updateTime = Column("DateModification", DateTime)
    updatedByCmsId = Column(
        "modifiedByCMSid", SmallInteger, ForeignKey(Person.cmsId), nullable=False
    )
    updater = relationship(Person, foreign_keys=[updatedByCmsId])
    voting = Column("WhichVote", String(50), nullable=False)
    fromCmsId = Column("CMSid", SmallInteger, ForeignKey(Person.cmsId), nullable=False)
    delegated_from = relationship(Person, foreign_keys=[fromCmsId])
    toCmsId = Column("toCMSid", SmallInteger, ForeignKey(Person.cmsId), nullable=False)
    delegated_to = relationship(Person, foreign_keys=[toCmsId])
    status = Column("Status", String(10), default="OK", nullable=False)


class PersonStatus(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = "PeopleStatusData"
    id = Column(
        "PrKeyPF",
        translate_type("int(11)"),
        nullable=translate_bool("NO"),
        primary_key=True,
    )
    cmsId = Column(
        "CMSid", translate_type("smallint(6)"), nullable=translate_bool("NO")
    )
    instCode = Column(
        "InstCode", translate_type("varchar(40)"), nullable=translate_bool("NO")
    )
    # sth = Column('ExtHRpersonId', translate_type('int(11)'), nullable=translate_bool('NO'))
    # sth = Column('ExtOrganization', translate_type('text'), nullable=translate_bool('NO'))
    # sth = Column('ExtDivGroup', translate_type('varchar(40)'), nullable=translate_bool('NO'))
    # sth = Column('ExtNameCERN', translate_type('varchar(40)'), nullable=translate_bool('NO'))
    # sth = Column('ExtNamfCERN', translate_type('varchar(40)'), nullable=translate_bool('NO'))
    # sth = Column('ExtEmailCERN', translate_type('varchar(60)'), nullable=translate_bool('NO'))
    statusConf = Column(
        "StatusConf", translate_type("varchar(30)"), nullable=translate_bool("NO")
    )
    function = Column("Function", translate_type("text"), nullable=translate_bool("NO"))
    project = Column(
        "Project", translate_type("varchar(30)"), nullable=translate_bool("NO")
    )
    year = Column("Year", StringInt(length=10), nullable=translate_bool("NO"))
    # sth = Column('sentMail', translate_type('varchar(250)'), nullable=translate_bool('YES'))
    # sth = Column('authorCMSid', translate_type('smallint(6)'), nullable=translate_bool('NO'))
    # sth = Column('authorConfDate', translate_type('varchar(50)'), nullable=translate_bool('NO'))
    # sth = Column('authorName', translate_type('varchar(200)'), nullable=translate_bool('NO'))
    # sth = Column('authorMail', translate_type('varchar(250)'), nullable=translate_bool('YES'))
    # sth = Column('secrCMSid', translate_type('smallint(6)'), nullable=translate_bool('NO'))
    # sth = Column('secrConfDate', translate_type('varchar(50)'), nullable=translate_bool('NO'))
    # sth = Column('secrName', translate_type('varchar(200)'), nullable=translate_bool('NO'))
    # sth = Column('memberCMSid', translate_type('smallint(6)'), nullable=translate_bool('NO'))
    # sth = Column('memberConfDate', translate_type('varchar(50)'), nullable=translate_bool('NO'))
    # sth = Column('memberName', translate_type('varchar(200)'), nullable=translate_bool('NO'))
    # sth = Column('memberMail', translate_type('varchar(250)'), nullable=translate_bool('YES'))
    # sth = Column('cbiCMSid', translate_type('smallint(6)'), nullable=translate_bool('NO'))
    # sth = Column('cbiConfDate', translate_type('varchar(50)'), nullable=translate_bool('NO'))
    cbiName = Column(
        "cbiName", translate_type("varchar(200)"), nullable=translate_bool("NO")
    )
    cbiMail = Column(
        "cbiMail", translate_type("varchar(250)"), nullable=translate_bool("YES")
    )
    # prjCMSid = Column('prjCMSid', translate_type('smallint(6)'), nullable=translate_bool('NO'))
    # sth = Column('prjConfDate', translate_type('varchar(50)'), nullable=translate_bool('NO'))
    # sth = Column('prjName', translate_type('varchar(200)'), nullable=translate_bool('NO'))
    # sth = Column('prjMail', translate_type('varchar(250)'), nullable=translate_bool('YES'))
    chairCmsId = Column(
        "chairCMSid", translate_type("smallint(6)"), nullable=translate_bool("NO")
    )
    chairConfDate = Column(
        "chairConfDate", translate_type("varchar(50)"), nullable=translate_bool("NO")
    )
    chairName = Column(
        "chairName", translate_type("varchar(200)"), nullable=translate_bool("NO")
    )
    chairMail = Column(
        "chairMail", translate_type("varchar(250)"), nullable=translate_bool("YES")
    )
    remarks = Column(
        "remarks", translate_type("varchar(250)"), nullable=translate_bool("YES")
    )
