#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re
from datetime import datetime, timedelta
from typing import Type

from sqlalchemy import Column, Enum, ForeignKey, func, orm, types
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import text

from icms_orm import (
    IcmsDeclarativeBasesFactory,
    PseudoEnum,
    toolkit_bind_key,
    toolkit_schema_name,
)
from icms_orm.common import (
    EmailMessage,
    Person,
    Institute,
    CmsActivityValues,
    Position,
    OrgUnit,
)
from icms_orm.orm_interface.model_base_class import IcmsModelBase

DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(
    toolkit_bind_key() or ""
)


class ToolkitBaseMixin:
    __bind_key__ = toolkit_bind_key()
    __table_args__ = {"schema": toolkit_schema_name()}


class Status(ToolkitBaseMixin, DeclarativeBase):

    ACTIVE = "active"
    CANCELLED = "cancelled"
    DELETED = "deleted"
    DISABLED = "disabled"
    PENDING = "pending"
    ACCEPTED = "accepted"
    REJECTED = "rejected"
    DONE = "done"
    SUSPENDED = "suspended"

    code = Column(types.String(32), primary_key=True)

    def __repr__(self):
        return self.code

    def __init__(self, code):
        self.code = code

    @staticmethod
    def get(code):
        return Status.query.filter(Status.code == code).one()


class CmsProject(ToolkitBaseMixin, DeclarativeBase):
    """
    More like a dictionary for drop-downs and similar.
    Can store unofficial projects which appear in some contexts, like room booking
    """

    code = Column(types.String(32), primary_key=True)
    name = Column(types.String(128), unique=True, nullable=False)
    official = Column(types.Boolean, default=True, nullable=False)
    status = Column(types.String(32), ForeignKey(Status.code, onupdate="CASCADE"))

    def __init__(self, code, name, status=Status.ACTIVE, official=True):
        self.code = code
        self.name = name
        self.status = status
        self.official = official


class CmsWeek(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    title = Column(types.String(80), nullable=False)
    date = Column(types.Date, nullable=False)
    date_end = Column(types.Date, nullable=False)
    is_external = Column(types.Boolean, nullable=False, default=False)

    def __init__(self, title=None, date=None, date_end=None, is_external=False):
        self.title = title
        self.date = date
        self.date_end = (
            date_end if date_end else (date + timedelta(days=4) if date else None)
        )
        self.is_external = is_external

    def __repr__(self):
        return "%s starting on %s" % (self.title, self.date)


class Voting(ToolkitBaseMixin, DeclarativeBase):
    class Type(PseudoEnum):
        VOTE = "VOTE"
        ELECTION = "ELECTION"

    id = Column(
        types.Integer, autoincrement=True, unique=True, nullable=False, primary_key=True
    )
    code = Column(types.String(16), primary_key=True, unique=True)
    title = Column(types.String(128), nullable=False)
    start_time = Column(types.DateTime, nullable=False)
    end_time = Column(types.DateTime, nullable=True)
    delegation_deadline = Column(types.DateTime, nullable=True)
    # spokesperson or some other chair elections have different rules - long term proxies do not apply
    type = Column(types.String(20), nullable=False, default=Type.VOTE)
    list_closing_date = Column(types.Date, nullable=True, default=None)
    applicable_mo_year = Column(types.Integer, nullable=True, default=None)

    def __init__(
        self,
        code=None,
        title=None,
        start_time=None,
        end_time=None,
        delegation_deadline=None,
        type=Type.VOTE,
    ):
        self.code = code
        self.title = title
        self.start_time = start_time
        self.end_time = end_time
        self.type = type
        self.delegation_deadline = delegation_deadline

    def __repr__(self):
        return "Voting %s [%s] voting to take place on %s" % (
            self.title,
            self.type,
            self.start_time,
        )


class VoteDelegation(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    voting_code = Column(
        types.String(16),
        ForeignKey(Voting.code, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=True,
    )
    cms_id_from = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    cms_id_to = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    status = Column(
        types.String(32), ForeignKey(Status.code, onupdate="CASCADE"), nullable=False
    )
    is_long_term = Column(types.Boolean, nullable=False, default=False)
    cms_id_creator = Column(types.Integer, nullable=False)
    time_created = Column(types.DateTime, default=func.now())
    cms_id_updater = Column(types.Integer, nullable=True)
    time_updated = Column(types.DateTime, onupdate=func.now())
    specific_inst_code = Column(
        types.String(32), ForeignKey(Institute.code), nullable=True
    )

    @classmethod
    def new(
        cls,
        voting_code,
        cms_id_from,
        cms_id_to,
        status,
        cms_id_creator=None,
        time_created=None,
        cms_id_updater=None,
        time_updated=None,
        is_long_term=False,
        specific_inst_code=None,
    ):
        this = cls()
        this.voting_code = voting_code
        this.cms_id_from = cms_id_from
        this.cms_id_to = cms_id_to
        this.status = status
        this.cms_id_creator = cms_id_creator
        if time_created:
            this.time_created = time_created
        this.cms_id_updater = cms_id_updater
        if time_updated:
            this.time_updated = time_updated
        this.is_long_term = is_long_term
        this.specific_inst_code = specific_inst_code
        return this


class CernCountryStatus(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    name = Column(types.String(64), nullable=False)

    def __init__(self, name):
        self.name = name


class CernCountry(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    name = Column(types.String(64), nullable=False)
    status_id = Column(
        types.SmallInteger,
        ForeignKey(CernCountryStatus.id, onupdate="CASCADE", ondelete="SET NULL"),
    )
    status = orm.relationship(CernCountryStatus)

    def __init__(self, name, status):
        self.name = name
        self.status = status


class ActivityCheck(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    for_cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
    )
    by_cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
    )
    for_activity = Column(types.SmallInteger)
    timestamp = Column(types.DateTime, default=datetime.utcnow)
    confirmed = Column(types.Boolean)

    def __init__(self, for_cms_id, by_cms_id, for_activity, confirmed, timestamp=None):
        self.for_cms_id = for_cms_id
        self.by_cms_id = by_cms_id
        self.for_activity = for_activity
        if timestamp is not None:
            self.timestamp = timestamp
        self.confirmed = confirmed


class ExtAuthorProject(ToolkitBaseMixin, DeclarativeBase):
    def __init__(self, name, active=True):
        self.name = name
        self.active = active

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    name = Column(types.String(128), nullable=False)
    active = Column(types.Boolean, nullable=False, default=True)


class ExtAuthorFlag(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    name = Column(types.String(128), nullable=False)
    project_id = Column(
        types.Integer,
        ForeignKey(ExtAuthorProject.id, ondelete="CASCADE"),
        nullable=False,
    )
    project = orm.relationship(ExtAuthorProject)
    active = Column(types.Boolean, nullable=False, default=True)
    authors = None


class ExtAuthor(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    name = Column(types.String(128), nullable=False)
    cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=True,
    )
    hr_id = Column(types.Integer, nullable=True)
    inst_code = Column(
        types.String(40),
        ForeignKey(Institute.code, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    inst_code_also = Column(
        types.String(40),
        ForeignKey(Institute.code, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=True,
    )
    inst_code_now = Column(
        types.String(40),
        ForeignKey(Institute.code, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=True,
    )
    reason = Column(types.Text, nullable=True)
    date_work_start = Column(types.Date, nullable=True)
    date_work_end = Column(types.Date, nullable=True)
    date_sign_end = Column(types.Date, nullable=True)
    project_id = Column(
        types.Integer,
        ForeignKey(ExtAuthorProject.id, ondelete="CASCADE"),
        nullable=False,
    )
    project = orm.relationship(ExtAuthorProject, backref="authors")
    spires_id = Column(types.String(40), nullable=True)
    flags = None


class ExtAuthorsFlagsRel(ToolkitBaseMixin, DeclarativeBase):

    # def __init__(self, author_id, flag_id):
    #     self.author_id = author_id
    #     self.flag_id = flag_id

    author_id = Column(
        types.Integer, ForeignKey(ExtAuthor.id, ondelete="CASCADE"), primary_key=True
    )
    author = orm.relationship(ExtAuthor, back_populates=ExtAuthor.flags)
    flag_id = Column(
        types.Integer,
        ForeignKey(ExtAuthorFlag.id, ondelete="CASCADE"),
        primary_key=True,
    )
    flag = orm.relationship(ExtAuthorFlag, back_populates=ExtAuthorFlag.authors)


ExtAuthor.flags = orm.relationship(ExtAuthorsFlagsRel)
ExtAuthorFlag.authors = orm.relationship(ExtAuthorsFlagsRel)

# ExtAuthor.flags = orm.relationship(ExtAuthorsFlagsRel, back_populates=ExtAuthorsFlagsRel.author)
# ExtAuthorFlag.authors = orm.relationship(ExtAuthorsFlagsRel, back_populates=ExtAuthorsFlagsRel.flag)


class Room(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    indico_id = Column(types.SmallInteger, nullable=True, unique=True)
    building = Column(types.String(8), nullable=False)
    floor = Column(types.String(4), nullable=False)
    room_nr = Column(types.String(8), nullable=False)
    custom_name = Column(types.String(128), nullable=True)
    at_cern = Column(types.Boolean, nullable=False, default=True)
    capacity = Column(types.SmallInteger, nullable=True)

    def __init__(
        self,
        indico_id=None,
        building=None,
        floor=None,
        room_nr=None,
        name=None,
        at_cern=True,
    ):
        self.indico_id = indico_id
        self.building = building
        self.floor = floor
        self.room_nr = room_nr
        if name is not None and name != "%s-%s-%s" % (building, floor, room_nr):
            self.custom_name = name
        self.at_cern = at_cern

    def __str__(self) -> str:
        return (
            not self.at_cern
            and self.custom_name
            or "{0}-{1}-{2}".format(self.building, self.floor, self.room_nr)
        )

    @staticmethod
    def get_room_by_string(s, session=None):
        pattern = r"(\w+)-(\w+)-(\w+)"
        m = re.search(pattern, s)
        if not m:
            return None
        else:
            room = (
                (session.query(Room) if session else Room.query)
                .filter(Room.building == m.group(1))
                .filter(Room.floor == m.group(2))
                .filter(Room.room_nr == m.group(3))
                .first()
            )
            return room


class RoomWeekRel(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    room_id = Column(
        types.SmallInteger,
        ForeignKey(Room.id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    week_id = Column(
        types.Integer,
        ForeignKey(CmsWeek.id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )


class RoomRequest(ToolkitBaseMixin, DeclarativeBase):
    @staticmethod
    def get_initialized(
        cms_id_by,
        title,
        webcast,
        time_start,
        duration,
        room_id_preferred,
        capacity,
        status,
        project,
        room_id=None,
        official=False,
        remarks=None,
        reason=None,
        password=None,
        cms_week_id=None,
        cms_id_for=None,
        time_created=None,
        time_updated=None,
        agenda_url=None,
    ):
        rr = RoomRequest()
        rr.cms_id_by = cms_id_by
        rr.cms_id_for = cms_id_for if cms_id_for else cms_id_by
        rr.title = title
        rr.webcast = webcast
        rr.time_start = time_start
        rr.duration = duration
        rr.room_id_preferred = room_id_preferred
        rr.room_id = room_id
        rr.capacity = capacity
        rr.status = status
        rr.official = official
        rr.remarks = remarks
        rr.reason = reason
        rr.password = password
        rr.cms_week_id = cms_week_id
        rr.project = project
        if time_created:
            rr.time_created = time_created
        if time_updated:
            rr.time_updated = time_updated
        rr.agenda_url = agenda_url
        return rr

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    cms_id_by = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    cms_id_for = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    title = Column(types.String(256), nullable=False)
    project = Column(
        types.String(32),
        ForeignKey(CmsProject.code, onupdate="CASCADE"),
        nullable=False,
    )
    webcast = Column(types.Boolean, default=False, nullable=False)
    time_start = Column(types.DateTime, nullable=False)
    duration = Column(types.SmallInteger, nullable=False, default=0)
    cms_week_id = Column(
        types.Integer, ForeignKey(CmsWeek.id, ondelete="CASCADE"), nullable=True
    )
    cms_week = orm.relationship(CmsWeek, lazy="joined")
    room_id_preferred = Column(
        types.SmallInteger, ForeignKey(Room.id), nullable=False, default=0
    )
    room_preferred = orm.relationship(
        Room, foreign_keys=[room_id_preferred], lazy="joined"
    )
    room_id = Column(types.SmallInteger, ForeignKey(Room.id), nullable=True)
    room = orm.relationship(Room, foreign_keys=[room_id], lazy="joined")

    capacity = Column(types.SmallInteger, default=10, nullable=False)
    password = Column(types.String(16), default=None, nullable=True)
    official = Column(types.Boolean, default=False, nullable=False)
    agenda_url = Column(types.String(256), default=None, nullable=True)
    remarks = Column(types.Text, nullable=True)
    reason = Column(types.Text, nullable=True)
    status = Column(
        types.String(32), ForeignKey(Status.code, onupdate="CASCADE"), nullable=True
    )

    time_created = Column(types.DateTime, default=func.now())
    time_updated = Column(types.DateTime, onupdate=func.now())

    def __unicode__(self):
        return 'Room request "%s" for room %s on %s' % (
            self.title,
            str(self.room_preferred),
            str(self.time_start),
        )


class RoomRequestConvener(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    request_id = Column(
        types.Integer, ForeignKey(RoomRequest.id, ondelete="CASCADE"), nullable=False
    )
    request = orm.relationship(
        RoomRequest, backref="weekly_meeting_conveners", lazy="joined"
    )
    active = Column(types.Boolean, nullable=False, default=True)

    def __init__(self, cms_id, request_id=None, request=None, active=True):
        if request:
            self.request = request
        else:
            self.request_id = request_id
        self.cms_id = cms_id
        self.active = active


class AuthorshipApplicationCheck(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    runNumber = Column("run_number", types.Integer, nullable=False)
    datetime = Column(types.DateTime, nullable=False, server_default=func.now())
    cmsId = Column(
        "cms_id",
        types.Integer,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    instCode = Column("inst_code", types.String(32), nullable=False)
    cmsActivity = Column("activity_id", types.Integer, nullable=False)
    passed = Column(types.Boolean, nullable=False)
    failed = Column(types.Boolean, nullable=False)
    reason = Column(types.String(128), nullable=True)
    daysCount = Column("days_count", types.Integer, nullable=False)
    workedSelf = Column("worked_self", types.Float, nullable=False)
    workedInst = Column("worked_inst", types.Float, nullable=False)
    neededInst = Column("needed_inst", types.Float, nullable=False)
    possibleSlipthrough = Column(
        "possible_slipthrough", types.Boolean, nullable=False, default=False
    )
    moStatus = Column("mo_status", types.Boolean, nullable=False, default=False)
    notificationId = Column(
        "notification_id",
        types.Integer,
        ForeignKey(EmailMessage.id, ondelete="SET NULL", onupdate="CASCADE"),
        nullable=True,
    )
    notifiactionMessage = orm.relationship(EmailMessage)

    def __init__(
        self,
        run_number,
        cms_id,
        inst_code,
        cms_activity,
        passed,
        failed,
        days,
        worked_self,
        worked_inst,
        needed_inst,
        possible_slipthrough,
        mo_status,
        reason=None,
    ):
        self.runNumber = run_number
        self.cmsId = cms_id
        self.instCode = inst_code
        self.cmsActivity = cms_activity
        self.passed = passed
        self.failed = failed
        self.daysCount = days
        self.workedInst = worked_inst
        self.workedSelf = worked_self
        self.neededInst = needed_inst
        self.possibleSlipthrough = possible_slipthrough
        self.moStatus = mo_status
        if reason:
            self.reason = reason


class VotingMerger(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    country = Column(types.String(64), nullable=False)
    valid_from = Column(types.Date, nullable=False)
    valid_till = Column(types.Date, nullable=True)
    representative_cms_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )

    def __init__(
        self, country=None, valid_from=None, valid_till=None, representative_cms_id=None
    ):
        self.country = country
        self.valid_from = valid_from
        self.valid_till = valid_till
        self.representative_cms_id = representative_cms_id


class VotingMergerMember(ToolkitBaseMixin, DeclarativeBase):
    merger_id = Column(
        types.Integer,
        ForeignKey(VotingMerger.id),
        primary_key=True,
        autoincrement=False,
    )
    merger = orm.relationship(VotingMerger, backref="members", lazy="joined")
    inst_code = Column(
        types.String(64),
        ForeignKey(Institute.code, onupdate="CASCADE", ondelete="CASCADE"),
        primary_key=True,
    )

    def __init__(self, inst_code, merger=None, merger_id=None):
        if merger:
            self.merger = merger
        elif merger_id:
            self.merger_id = merger_id
        self.inst_code = inst_code


class VotingListEntry(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, autoincrement=True, primary_key=True)
    code = Column(
        types.String(16),
        ForeignKey(Voting.code, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    cms_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    weight = Column(types.Float, nullable=False, default=1.0)
    represented_inst_code = Column(
        types.String(32),
        ForeignKey(Institute.code, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=True,
    )
    represented_merger_id = Column(
        types.Integer,
        ForeignKey(VotingMerger.id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=True,
    )
    represented_merger = orm.relationship(VotingMerger)
    delegated_by_cms_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=True,
    )
    remarks = Column(types.Text, nullable=True)
    timestamp = Column(types.DateTime, nullable=False, server_default=func.now())


class ALFileset(ToolkitBaseMixin, DeclarativeBase):
    __tablename__ = "al_fileset"

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    paper_code = Column(types.String(64), nullable=False, unique=True)
    location = Column(types.String(64), nullable=False, default="eos")
    last_update = Column(
        types.Date,
        nullable=False,
        default=datetime.utcnow,
        server_default=text("timezone('UTC', now())"),
    )

    def __init__(self, paper_code=None, location="eos", last_update=None):
        self.paper_code = paper_code
        self.location = location
        self.last_update = last_update if last_update else datetime.utcnow()


class RestrictedResourceTypeValues(PseudoEnum):
    FILE = "file"
    DB_ROW = "database row"
    DB_TABLE = "database table"
    ENDPOINT = "endpoint"


class RestrictedActionTypeValues(PseudoEnum):
    READ = "read"
    WRITE = "write"
    DELETE = "delete"

    @classmethod
    def get_for_http_method_name(cls, http_method_name: str) -> str:
        action = {
            "get": RestrictedActionTypeValues.READ,
            "delete": RestrictedActionTypeValues.DELETE,
            "post": RestrictedActionTypeValues.WRITE,
            "put": RestrictedActionTypeValues.WRITE,
            "patch": RestrictedActionTypeValues.WRITE,
        }.get(http_method_name.lower())
        return action


resource_type_enum = Enum(
    *RestrictedResourceTypeValues.values(),
    name="restricted_resource_type_enum",
    schema=toolkit_schema_name(),
    metadata=DeclarativeBase.metadata
)
restricted_action_enum = Enum(
    *RestrictedActionTypeValues.values(),
    name="restricted_action_type_enum",
    schema=toolkit_schema_name(),
    metadata=DeclarativeBase.metadata
)


class AccessClass(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    name = Column(types.String(128), unique=True, nullable=False)
    # Arrays will be expanded into alternatives, maps into conjunctions, eg.:
    # the following: [{cms_id: [9981, 3040], inst_code: 'CERN'}, {cms_id: 1}]
    # will be converted into: (cms_id == 9981 or cms_id == 3040) and inst_code == 'CERN' or cms_id == 1
    rules = Column(JSONB)


class RestrictedResource(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    type = Column(resource_type_enum, nullable=False)
    key = Column(types.String(128), nullable=False, unique=True)
    # Arrays - alternatives, maps - conjunctions
    filters = Column(JSONB)


class Permission(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    resource_id = Column(
        types.Integer,
        ForeignKey(RestrictedResource.id, ondelete="CASCADE", onupdate="CASCADE"),
    )
    resource = relationship(RestrictedResource)
    access_class_id = Column(
        types.Integer,
        ForeignKey(AccessClass.id, onupdate="CASCADE", ondelete="CASCADE"),
    )
    access_class = relationship(AccessClass)
    action = Column(restricted_action_enum, nullable=False)


class ProvidedIdentity(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, primary_key=True, autoincrement=True)
    timestamp = Column(types.DateTime, default=datetime.utcnow)
    token = Column(types.String(64), nullable=False)
    requested_url = Column(types.Text, nullable=False)
    hr_id = Column(types.Integer)
    username = Column(types.String(64), nullable=False)


################
# Award models #
################

person_activity_enum = Enum(
    *CmsActivityValues.values(),
    name="toolkit_person_activity_enum",
    schema=toolkit_schema_name()
)


class AwardType(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, autoincrement=True, primary_key=True)
    type = Column(types.String(200), nullable=False, unique=True)


class Award(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, autoincrement=True, primary_key=True)
    award_type_id = Column(
        types.Integer, ForeignKey(AwardType.id, onupdate="CASCADE"), nullable=False
    )
    year = Column(types.Integer, nullable=False)
    nominations_open_date = Column(types.Date, nullable=False)
    nominations_deadline = Column(types.Date, nullable=False)
    awardee_publication_date = Column(types.Date, nullable=True)
    remarks = Column(types.String(1000), nullable=True)
    is_active = Column(types.Boolean, nullable=False, default=True)
    last_updated_at = Column(
        types.DateTime,
        nullable=False,
        default=datetime.utcnow,
        onupdate=datetime.utcnow,
    )
    last_updated_by = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=False
    )


class AwardNomination(ToolkitBaseMixin, DeclarativeBase):
    nomination_id = Column(types.Integer, autoincrement=True, primary_key=True)
    award_id = Column(
        types.Integer, ForeignKey(Award.id, onupdate="CASCADE"), nullable=False
    )
    nominator_cms_id = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=False
    )
    nominee_cms_id = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=True
    )
    nominee_activity = Column(person_activity_enum, nullable=True)
    nomination_projects = Column(types.String(64), nullable=False)
    nominee_institute = Column(
        types.String(32), ForeignKey(Institute.code, onupdate="CASCADE"), nullable=True
    )
    nominee_phd_date = Column(types.Date, nullable=True)
    working_relationship = Column(types.String(1000), nullable=False)
    justification = Column(types.String(1000), nullable=False)
    proposed_citation = Column(types.String(1000), nullable=True)
    remarks = Column(types.String(1000), nullable=True)
    is_active = Column(types.Boolean, nullable=False, default=True)
    last_updated_at = Column(
        types.DateTime, nullable=False, server_default=text("timezone('UTC', now())")
    )
    last_updated_by = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=False
    )
    # TBD: Are awards to external organizations possible? What are the requirements?
    # external_organization = Column(types.String(128), nullable=True)


class Awardee(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.Integer, autoincrement=True, primary_key=True)
    award_id = Column(
        types.Integer, ForeignKey(Award.id, onupdate="CASCADE"), nullable=False
    )
    cms_id = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=True
    )
    activity = Column(person_activity_enum, nullable=True)
    project = Column(types.String(64), nullable=False)
    institute = Column(
        types.String(32), ForeignKey(Institute.code, onupdate="CASCADE"), nullable=True
    )
    citation = Column(types.String(1000), nullable=False)
    remarks = Column(types.String(1000), nullable=True)
    is_active = Column(types.Boolean, nullable=False, default=True)
    last_updated_at = Column(
        types.DateTime, nullable=False, server_default=text("timezone('UTC', now())")
    )
    last_updated_by = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=False
    )
    first_name = Column(types.String(255), nullable=True)
    last_name = Column(types.String(255), nullable=True)
    # TBD: Are awards to external organizations possible? What are the requirements?
    # external_organization = Column(types.String(128), nullable=True)


###########################
# Weekly Meetings Models #
###########################


class OddEvenWeekly(PseudoEnum):
    WEEKLY = "weekly"
    ODD = "odd"
    EVEN = "even"


class DayOfWeek(PseudoEnum):
    MONDAY = "monday"
    TUESDAY = "tuesday"
    WEDNESDAY = "wednesday"
    THURSDAY = "thursday"
    FRIDAY = "friday"
    SATURDAY = "saturday"
    SUNDAY = "sunday"


class WeeklyMeetingState(PseudoEnum):
    PROPOSED = "proposed"
    APPROVED = "approved"
    CANCELLED = "cancelled"


odd_even_weekly_enum = Enum(
    *OddEvenWeekly.values(),
    name="odd_even_weekly_enum",
    schema=toolkit_schema_name(),
    metadata=DeclarativeBase.metadata
)
day_of_week_enum = Enum(
    *DayOfWeek.values(),
    name="day_of_week_enum",
    schema=toolkit_schema_name(),
    metadata=DeclarativeBase.metadata
)
state_enum = Enum(
    *WeeklyMeetingState.values(),
    name="state_enum",
    schema=toolkit_schema_name(),
    metadata=DeclarativeBase.metadata
)


class WeeklyMeetings(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    title = Column(types.String(128), nullable=False)
    odd_even_weekly = Column(odd_even_weekly_enum, nullable=False)
    day_of_week = Column(day_of_week_enum, nullable=False)
    start_time = Column(types.Time, nullable=False)
    end_time = Column(types.Time, nullable=False)
    room = Column(types.String(80), nullable=False)
    primary_contact = Column(types.String(128), nullable=True)
    state = Column(state_enum, nullable=False)

    def __init__(
        self,
        title,
        odd_even_weekly,
        day_of_week,
        start_time,
        end_time,
        room,
        state,
        primary_contact=None,
    ):
        self.title = title
        self.odd_even_weekly = odd_even_weekly
        self.day_of_week = day_of_week
        self.start_time = start_time
        self.end_time = end_time
        self.room = room
        self.primary_contact = primary_contact
        self.state = state

    def __repr__(self):
        return (
            "Title: %s, Odd-Even-Weekly: %s, "
            "Day: %s, Start-Ending time: %s %s, "
            "Room: %s, Primary Contact: %s, State: %s, "
            "Weekly-Meeting-Conveners: %s "
            % (
                self.title,
                self.odd_even_weekly,
                self.day_of_week,
                self.start_time,
                self.end_time,
                self.room,
                self.primary_contact,
                self.state,
                self.weekly_meeting_conveners,
            )
        )


class WeeklyMeetingsConvener(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    weekly_meetings_id = Column(
        types.Integer, ForeignKey(WeeklyMeetings.id, ondelete="CASCADE"), nullable=False
    )

    weekly_meeting = relationship(
        "WeeklyMeetings", back_populates="weekly_meeting_conveners"
    )

    def __init__(self, cms_id, weekly_meetings_id=None):
        self.cms_id = cms_id
        self.weekly_meetings_id = weekly_meetings_id


WeeklyMeetings.weekly_meeting_conveners = relationship(
    "WeeklyMeetingsConvener",
    order_by=WeeklyMeetingsConvener.id,
    back_populates="weekly_meeting",
)


###########################
# PhD Info Models #
###########################
class PhdInfo(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    thesis_title = Column(types.String(10000), nullable=True)
    thesis_link = Column(types.String(10000), nullable=True)
    cms_id = Column(
        types.SmallInteger,
        ForeignKey(Person.cms_id, onupdate="CASCADE", ondelete="RESTRICT"),
        nullable=False,
    )
    student_institute = Column(
        types.String,
        ForeignKey(Institute.code, onupdate="CASCADE", ondelete="RESTRICT"),
        nullable=False,
    )
    enrollment_date = Column(types.Date, nullable=True)
    dissertation_defense_date = Column(types.Date, nullable=True)
    physics_groups = Column(types.String(10000), nullable=True)
    cms_projects = Column(types.String(10000), nullable=True)
    cadi_analyses = Column(types.String(10000), nullable=True)
    cms_notes = Column(types.String(10000), nullable=True)
    remarks = Column(types.String(10000), nullable=True)
    is_active = Column(types.Boolean, default=True, nullable=False)
    last_updated_at = Column(
        types.DateTime,
        nullable=False,
        server_default=text("timezone('UTC', now())"),
        onupdate=datetime.utcnow,
    )
    last_updated_by = Column(
        types.Integer, ForeignKey(Person.cms_id, onupdate="CASCADE"), nullable=False
    )

    student = relationship(
        Person, foreign_keys=[cms_id], lazy="joined", innerjoin="True"
    )
    updater = relationship(
        Person, foreign_keys=[last_updated_by], lazy="joined", innerjoin="True"
    )

    def _init(
        self,
        cms_id,
        student_institute,
        last_updated_by,
        enrollment_date=None,
        thesis_title=None,
        thesis_link=None,
        dissertation_defense_date=None,
        physics_groups=None,
        cms_projects=None,
        cadi_analyses=None,
        cms_notes=None,
        remarks=None,
        is_active=True,
    ):
        self.cms_id = cms_id
        self.student_institute = student_institute
        self.last_updated_by = last_updated_by
        self.enrollment_date = enrollment_date
        self.thesis_title = thesis_title
        self.thesis_link = thesis_link
        self.dissertation_defense_date = dissertation_defense_date
        self.physics_groups = physics_groups
        self.cms_projects = cms_projects
        self.cadi_analyses = cadi_analyses
        self.cms_notes = cms_notes
        self.remarks = remarks
        self.is_active = is_active


###########################
# JobQuestionnaire Model #
###########################


class JobQuestionnaire(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    title = Column(types.String(200), nullable=False)
    questions = Column(types.String(4000), nullable=False)
    date_created = Column(types.DateTime, nullable=True, default=datetime.utcnow)
    date_updated = Column(
        types.DateTime, nullable=True, default=datetime.utcnow, onupdate=datetime.utcnow
    )


###########################
# JobOpening Model #
###########################


class JobOpening(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    title = Column(types.String(200), nullable=False)
    description = Column(types.String(4000), nullable=False)  # or mandate
    requirements = Column(types.String(4000), nullable=False)  # on the candidate
    start_date = Column(types.Date, nullable=False)
    end_date = Column(types.Date, nullable=True)
    nominations_deadline = Column(types.Date, nullable=False)
    days_for_nominee_response = Column(types.Integer, nullable=True)
    nominee_no_reply = Column(
        types.Boolean, nullable=True
    )  # , comment='use to store the value("True(acceptance)", "False(rejection)" or "Null") when nominee does not reply within the deadline')
    status = Column(types.String(80), nullable=False)
    comment = Column(types.String(1000), nullable=True)
    send_mail_to_nominee = Column(types.Boolean, nullable=True)
    questionnaire_uri = Column(types.String(500), nullable=True)
    last_modified = Column(
        types.DateTime,
        nullable=False,
        default=datetime.utcnow,
        onupdate=datetime.utcnow,
    )
    questionnaire_id = Column(
        types.Integer,
        ForeignKey(JobQuestionnaire.id, onupdate="CASCADE"),
        nullable=True,
    )
    notify_egroups = Column(types.String(1000), nullable=True)


###########################
# JobOpenPosition Model #
###########################


class JobOpenPosition(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    position_id = Column(
        types.SmallInteger, ForeignKey(Position.id, onupdate="CASCADE"), nullable=False
    )
    position = relationship(Position, lazy="joined")
    job_unit_id = Column(
        types.SmallInteger, ForeignKey(OrgUnit.id, onupdate="CASCADE"), nullable=False
    )
    job_unit = relationship(OrgUnit, lazy="joined")
    job_opening_id = Column(
        types.SmallInteger,
        ForeignKey(JobOpening.id, onupdate="CASCADE"),
        nullable=False,
    )
    job_opening = relationship(JobOpening, lazy="joined")
    start_date = Column(types.Date, nullable=False)
    end_date = Column(types.Date, nullable=True)
    nominations_deadline = Column(types.Date, nullable=False)
    status = Column(types.String(128), nullable=False)


# ###########################
# # JobNomination Models #
# ###########################


class JobNomination(ToolkitBaseMixin, DeclarativeBase):

    nomination_id = Column(types.Integer, primary_key=True, autoincrement=True)
    open_position_id = Column(
        types.Integer,
        ForeignKey(JobOpenPosition.id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    open_position = relationship(JobOpenPosition)
    nominee_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="SET NULL", onupdate="CASCADE"),
        nullable=False,
    )
    nominee = relationship(Person)
    questionnaire_answers = Column(types.String(4000), nullable=True)


class JobNominationStatus(ToolkitBaseMixin, DeclarativeBase):

    id = Column(types.Integer, primary_key=True, autoincrement=True)
    actor_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="SET NULL", onupdate="CASCADE"),
        nullable=False,
    )
    nomination_id = Column(
        types.Integer,
        ForeignKey(JobNomination.nomination_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    actor_remarks = Column(types.String(1000), nullable=True)
    status = Column(types.String(128), nullable=False)
    last_modified = Column(
        types.DateTime,
        nullable=False,
        default=datetime.utcnow,
        onupdate=datetime.utcnow,
    )
    nomination = relationship(JobNomination)


###################################
# JobQuestionnaireAnswers Model #
###################################


class JobQuestionnaireAnswers(ToolkitBaseMixin, DeclarativeBase):
    id = Column(types.SmallInteger, primary_key=True, autoincrement=True)
    answers = Column(types.String(10000), nullable=False)
    questionnaire_id = Column(
        types.SmallInteger,
        ForeignKey(JobQuestionnaire.id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    job_nomination_id = Column(
        types.SmallInteger,
        ForeignKey(JobNomination.nomination_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    submitter_id = Column(
        types.Integer,
        ForeignKey(Person.cms_id, ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    submitted_at = Column(
        types.DateTime, nullable=True, default=datetime.utcnow, onupdate=datetime.utcnow
    )
