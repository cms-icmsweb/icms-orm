#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0
import os
import re
import logging
import sqlalchemy as sa
import stringcase

from collections import OrderedDict
from sqlalchemy import MetaData

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.attributes import InstrumentedAttribute

from icms_orm.orm_interface import IcmsModelBase
from icms_orm.orm_interface import IcmsDeclarativeBasesFactory
from icms_orm.orm_interface import OrmManager

from icms_orm.custom_classes import OverridableConfigOption
from icms_orm.custom_classes import PseudoEnum
from icms_orm.custom_classes import DatabaseViewMixin

from icms_orm.custom_methods import *


implicit_return_val = OverridableConfigOption(lambda: 'TEST_DATABASE_URL' in os.environ.keys())

# Bind keys
toolkit_bind_key = OverridableConfigOption('icms_toolkit_db')
epr_bind_key = OverridableConfigOption('icms_epr_db')
cms_people_bind_key = OverridableConfigOption('icms_legacy_db')
cadi_bind_key = OverridableConfigOption(cms_people_bind_key)
old_notes_bind_key = OverridableConfigOption(cms_people_bind_key)
old_wf_notes_bind_key = OverridableConfigOption(old_notes_bind_key)
metadata_bind_key = OverridableConfigOption(cms_people_bind_key)
news_bind_key = OverridableConfigOption(cms_people_bind_key)
newcadi_bind_key = OverridableConfigOption('icms_cadi_db')
cms_common_bind_key = OverridableConfigOption('icms_common_db')
notes_bind_key = OverridableConfigOption('notes')

# Schema names
cms_common_schema_name = OverridableConfigOption('public', env_override_key='ICMS_COMMON_SCHEMA')
toolkit_schema_name = OverridableConfigOption('toolkit', env_override_key='ICMS_TOOLKIT_SCHEMA_NAME')
newcadi_schema_name = OverridableConfigOption('cadi', env_override_key='ICMS_CADI_SCHEMA')
notes_schema_name = OverridableConfigOption('notes', env_override_key='ICMS_NOTES_SCHEMA')
epr_schema_name = OverridableConfigOption('epr', env_override_key='ICMS_EPR_SCHEMA_NAME')

cms_people_schema_name = OverridableConfigOption('CMSPEOPLE', env_override_key='ICMS_OLD_PEOPLE_SCHEMA')
cadi_schema_name = OverridableConfigOption('CMSAnalysis', env_override_key='ICMS_OLD_CADI_SCHEMA')
old_notes_schema_name = OverridableConfigOption('Portal_notes', env_override_key='ICMS_OLD_NOTES_SCHEMA')
old_notes_wf_schema_name = OverridableConfigOption('Portal_wf_notes', env_override_key='ICMS_OLD_WF_NOTES_SCHEMA')
metadata_schema_name = OverridableConfigOption('metadata', env_override_key='ICMS_OLD_METADATA_SCHEMA')
news_schema_name = OverridableConfigOption('Portal_news', env_override_key='ICMS_OLD_NEWS_SCHEMA')

# User and role names
toolkit_role_name = OverridableConfigOption('toolkit', env_override_key='ICMS_TOOLKIT_ROLE')
epr_role_name = OverridableConfigOption('epr', env_override_key='ICMS_EPR_ROLE')
cms_common_role_name = OverridableConfigOption('icms', env_override_key='ICMS_COMMON_ROLE')
icms_reader_role = OverridableConfigOption('icms_reader', env_override_key='ICMS_READER_ROLE')
icms_legacy_user = OverridableConfigOption('icms_legacy', env_override_key='ICMS_LEGACY_USER')

# DB Server hosts - in most cases the defaults will do but in CI they need to be swapped easily
postgres_host = OverridableConfigOption('localhost', 'POSTGRES_HOST')
mysql_host = OverridableConfigOption('localhost', 'MYSQL_HOST')